package com.molpay.molpaylib.transaction;

import android.test.AndroidTestCase;

import com.google.zxing.FormatException;
import com.molpay.molpaylib.JSONnString;

import org.json.JSONStringer;

/**
 * Created by medetzhakupov on 5/28/15.
 */
public class TestTransaction extends AndroidTestCase {

    final String merchantId = "molpaymerchant";
    final String transactionId = "4693795";
    final String orderId = "GPAA29452";
    final float amount = 1.01f;
    final String verifyKey = "501c4f508cf1c3f486f4f5c820591f41";
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        String str = generateEANContentChecksum("000004710587");
//        String result = JSONnString.requestResult(merchantId, transactionId, amount, verifyKey);
        String result = JSONnString.GetTxnIdManually(merchantId, orderId, amount, verifyKey);


        assertNotNull("Result is nut null", result);

    }

    String generateEANContentChecksum(CharSequence s) throws FormatException {
        int length = s.length();
        if (length == 0) {
            return "";
        }

        if (s.length() != 12)
            throw new IllegalArgumentException();

        s = s + "0";
        length = s.length();

        int sum = 0;
        for (int i = length - 2; i >= 0; i -= 2) {
            int digit = (int) s.charAt(i) - (int) '0';
            if (digit < 0 || digit > 9) {
                throw FormatException.getFormatInstance();
            }
            sum += digit;
        }
        sum *= 3;
        for (int i = length - 1; i >= 0; i -= 2) {
            int digit = (int) s.charAt(i) - (int) '0';
            if (digit < 0 || digit > 9) {
                throw FormatException.getFormatInstance();
            }
            sum += digit;
        }
        int remain = sum % 10;
        if (remain > 0)
            s = s.subSequence(0, s.length() - 1) + "" + (10 - remain);

        return s.toString();
    }


}
