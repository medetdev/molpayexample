package com.molpay.molpaylib;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.molpay.molpaylib.settings.MerchantInfo;
import com.molpay.molpaylib.utilities.CustomTimer;
import com.molpay.molpaylib.utilities.SecurePreferences;
import com.molpay.molpaylib.utilities.Utils;
import com.molpay.molpaylib.utilities.Utils.ResourceType;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import android.widget.ZoomButtonsController;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PaymentWebViewFrag extends Fragment {
	private static final String SECRET_KEY = "e1f5b6d6f0f1bc13d70029da4284c1be018c6151";
	public static final String TAG_LOG = PaymentWebViewFrag.class.getSimpleName();

	// OrderInfo
	private String OrderId;
	private float Amount;
	// private String Country;
	// private String Currency;
	private boolean secureMode;
	 private String Channel;
	 private String bill_name;
	 private String bill_desc;
	 private String bill_email;
	 private String bill_mobile;
	private String transactionId;
	// MerchantInfo
	private String MerchantId;
	// private String Password;
	private String VerifyKey;
	int click = 0;
	// private String AppName;

	// private Button proceed;
	WebView mWebView;
	Bundle extras;
	// private ZoomButtonsController zoom_controll = null;
	ArrayList<String> urls = new ArrayList<String>();
	ArrayList<WebView> webs = new ArrayList<WebView>();
	WebView childView;
	private static final FrameLayout.LayoutParams FILL = new FrameLayout.LayoutParams(
			ViewGroup.LayoutParams.MATCH_PARENT,
			ViewGroup.LayoutParams.MATCH_PARENT);
	private String exitURL = "https://www.onlinepayment.com.my/NBepay/result.php";
	private String exitURL2 = "https://www.onlinepayment.com.my/MOLPay/result.php";
	private String request = "https://www.onlinepayment.com.my/MOLPay/API/mobile_new/index.php";
	private LinearLayout li;
	private FrameLayout titlebar;
	private ProgressDialog progressDialog;
	private AlertDialog alert, pendingAlert;
	private Bundle bundle;
	boolean cameBefore;
	   private Bundle webViewBundle;
	   private String Currency;
	   int backCount;

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		Bundle extras = this.getArguments().getBundle("bundle");
	 	String HTMLResult = this.getArguments().getString("web");
	 
		if (extras != null) {
			secureMode = extras.getBoolean("IsSecure", false);
			MerchantId = extras.getString("MerchantId");
			VerifyKey = extras.getString("VerifyKey");
			OrderId = extras.getString("OrderId");
			Amount = extras.getFloat("Amount", 0.0f);
			bill_name = extras.getString("BillName");
			bill_desc = extras.getString("BillDesc");
			bill_mobile = extras.getString("BillMobile");
			bill_email = extras.getString("BillEmail");
			Channel=extras.getString("Channel", "multi");
			Currency=extras.getString("Currency", "MYR");
		}


		li = new LinearLayout(getActivity());
		li.setLayoutParams(FILL);
		li.setOrientation(LinearLayout.VERTICAL);
		setUpWebView();	 
		exitDialog();
		DialogExit();
		int paddingPixel = 10;
		float density = getActivity().getApplicationContext().getResources()
				.getDisplayMetrics().density;
		int paddingDp = (int) (paddingPixel * density);

		titlebar = new FrameLayout(getActivity());

		FrameLayout.LayoutParams params1 = new FrameLayout.LayoutParams(
				FrameLayout.LayoutParams.WRAP_CONTENT,
				FrameLayout.LayoutParams.WRAP_CONTENT,
				Gravity.CENTER);

		ImageView imgV = new ImageView(getActivity());
		imgV.setLayoutParams(params1);

		FrameLayout.LayoutParams params2 = new FrameLayout.LayoutParams(
				FrameLayout.LayoutParams.WRAP_CONTENT,
				FrameLayout.LayoutParams.MATCH_PARENT);
		params2.gravity = Gravity.LEFT | Gravity.CENTER_VERTICAL;
        params2.setMargins((int) (5 * density), 0, 0, 0);
		ImageView imgVBack = new ImageView(getActivity());

		imgVBack.setBackground(ContextCompat.getDrawable(getActivity(), Utils.getInstance().getResource(getActivity(), "back_button", ResourceType.drawable)));
		imgVBack.setImageResource(Utils.getInstance().getResource(getActivity(), "ic_menu_back", ResourceType.drawable));
		imgVBack.setLayoutParams(params2);
		imgV.setImageResource(Utils.getInstance().getResource(getActivity(), "molpay_logo", ResourceType.drawable));

		imgV.setPadding(0, paddingDp, 0, paddingDp);

		titlebar.setBackgroundColor(getResources().getColor(Utils.getInstance().getResource(getActivity(), "actionBarColor", ResourceType.color)));
		titlebar.addView(imgVBack);
		titlebar.addView(imgV);
		titlebar.setMinimumHeight(50);

//		li.addView(titlebar);
		li.addView(mWebView);
		 
	 
    	mWebView.loadData(HTMLResult, "text/html; charset=UTF-8", null);
		    

		if (webs != null)
        	webs.add(mWebView);
		return li;

		// return v;
	}
	
	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
		mWebView = null;
		webs = null;
	}

	 

	@SuppressLint("SetJavaScriptEnabled")
	private void setUpWebView() {
		final float ORIG_APP_W = 700;
		// float ORIG_APP_H = 800;

		mWebView = new WebView(getActivity());
		mWebView.getSettings().setJavaScriptEnabled(true);

		ProgressBar p = new ProgressBar(getActivity());

		mWebView.clearFormData();
		mWebView.clearHistory();
		mWebView.clearCache(true);

		mWebView.getSettings().setAppCacheEnabled(true);
		mWebView.setLayoutParams(FILL);
		// mWebView.setVisibility(View.INVISIBLE);

		//mWebView.getSettings().setPluginsEnabled(true);
		mWebView.getSettings().setPluginState(PluginState.ON);
		mWebView.getSettings().setAllowFileAccess(true);

		mWebView.getSettings().setSupportMultipleWindows(true);
		// get actual screen size
		Display display = ((WindowManager) getActivity().getSystemService(
				Context.WINDOW_SERVICE)).getDefaultDisplay();
		int width = display.getWidth();
		int height = display.getHeight();

		// calculate target scale (only dealing with portrait orientation)
		double globalScale = Math.ceil((width / ORIG_APP_W) * 100);
		mWebView.getSettings().setBuiltInZoomControls(false);
		mWebView.getSettings().setSupportZoom(true);
		mWebView.getSettings().setGeolocationEnabled(true);
		mWebView.getSettings().setLightTouchEnabled(true);
		mWebView.getSettings().setRenderPriority(RenderPriority.HIGH);
		mWebView.getSettings().setDomStorageEnabled(true);
		mWebView.getSettings().setBuiltInZoomControls(true);

		// mWebView.getSettings().setDisplayZoomControls(false);
		// set the scale
		mWebView.setInitialScale((int) globalScale);
		 mWebView.getSettings().setUseWideViewPort(true);
		mWebView.getSettings().setLoadWithOverviewMode(true);
		mWebView.getSettings().setSavePassword(false);
		mWebView.getSettings().setSaveFormData(true);

		mWebView.getSettings().setDatabaseEnabled(true);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

		mWebView.setOnKeyListener(new OnKeyListener() {

			public boolean onKey(View v, int keyCode, KeyEvent event) {
				backCount++;
				if ((keyCode == KeyEvent.KEYCODE_BACK)) {

					if(mWebView.canGoBack() && backCount==1)
					{

					mWebView.goBack();
					backCount--;
					return true;
					}
					else if(backCount==1)
					{
						backCount--;

					alert.show();
					return true;
					}

				}
				return false;
			}

		});

		mWebView.getSettings().setAllowFileAccess(true);
		mWebView.setWebViewClient(new AppWebViewClients(p));
		WebChromeClient wbc = new WebChromeClient() {
			@Override
			public boolean onCreateWindow(WebView view, boolean isDialog,
					boolean isUserGesture, Message resultMsg) {
				// TODO Auto-generated method stub

				childView = new WebView(view.getContext());
				final WebSettings settings = childView.getSettings();
				settings.setJavaScriptEnabled(true);
				ProgressBar p = new ProgressBar(getActivity());
				childView.setWebViewClient(new AppWebViewClients(p));
				childView.setWebChromeClient(this);

				childView.getSettings().setSupportMultipleWindows(true);

				childView.setLayoutParams(FILL);
				childView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);


				//childView.getSettings().setPluginsEnabled(true);
				mWebView.getSettings().setPluginState(PluginState.ON);
				childView.getSettings().setAllowFileAccess(true);
				childView.getSettings().setSupportMultipleWindows(true);
				// get actual screen size
				Display display = ((WindowManager) getActivity()
						.getSystemService(Context.WINDOW_SERVICE))
						.getDefaultDisplay();
				int width = display.getWidth();
				int height = display.getHeight();


				double globalScale = Math.ceil((width / ORIG_APP_W) * 100);
				childView.getSettings().setBuiltInZoomControls(false);
				childView.getSettings().setSupportZoom(true);
				childView.getSettings().setGeolocationEnabled(true);
				childView.getSettings().setLightTouchEnabled(true);
				childView.getSettings().setRenderPriority(RenderPriority.HIGH);
				childView.getSettings().setDomStorageEnabled(true);
				childView.getSettings().setBuiltInZoomControls(true);
				childView.setOnKeyListener(new OnKeyListener() {

					@SuppressLint("NewApi")
					public boolean onKey(View v, int keyCode, KeyEvent event) {
						if ((keyCode == KeyEvent.KEYCODE_BACK)) {

							if(childView.canGoBack()){
								childView.goBack();
								return true;
			                }else{
			                 alert.show();
			             	return true;
			                }

						}
						return false;
					}

				});
			//	childView.getSettings().setDisplayZoomControls(false);
				// set the scale
			      childView.setInitialScale((int) globalScale);
				 mWebView.getSettings().setUseWideViewPort(true);
				childView.getSettings().setLoadWithOverviewMode(true);
				childView.getSettings().setSavePassword(false);
				childView.getSettings().setSaveFormData(false);

				childView.getSettings().setDatabaseEnabled(true);
				li.addView(childView);

				WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
				transport.setWebView(childView);
				resultMsg.sendToTarget();
				childView.requestFocus();
				webs.add(view);

				view.setVisibility(View.GONE);

				return true;
			}

			@Override
			public void onCloseWindow(WebView window) {
				// TODO Auto-generated method stub
				super.onCloseWindow(window);
				try
				{
				li.removeViewAt(li.getChildCount() - 1);
				WebView w = webs.get(webs.size() - 1);
			    Display display = ((WindowManager) getActivity()
						.getSystemService(Context.WINDOW_SERVICE))
						.getDefaultDisplay();
				int width = display.getWidth();
				int height = display.getHeight();
				double globalScale = Math.ceil((width / ORIG_APP_W) * 100);
				w.setInitialScale((int) globalScale);
				w.requestFocus();
				w.setVisibility(View.VISIBLE);
				if(Channel.equals("cimb")||Channel.equals("hlb") || Channel.equals("maybank2u"))
				{
					w.loadUrl("javascript:actionForWindowClose()");
				}
				}
				catch
				(Exception Ex)
				{

				}

			}

		};

		mWebView.setWebChromeClient(wbc);
	}

	public class AppWebViewClients extends WebViewClient {
		private ProgressBar progressBar;

		public AppWebViewClients(ProgressBar progressBar) {
			this.progressBar = progressBar;
			progressBar.setVisibility(View.VISIBLE);

		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			// TODO Auto-generated method stub

			view.loadUrl(url);
			return true;
			// return false;

		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onPageFinished(view, url);

			progressBar.setVisibility(View.GONE);

			boolean found = false;
			for (int i = 0; i < urls.size(); i++) {
				String u = urls.get(i);
				if (u.equals(url)) {
					found = true;
				}

			}
			if (!found) {
				urls.add(url);
			}

		
			if (url.contains(exitURL) || url.contains(exitURL2)) {
				// mWebView.setVisibility(View.INVISIBLE);
				// li.setVisibility(View.INVISIBLE);
				// try{
				// // childView.setVisibility(View.INVISIBLE);
				// }
				// finally{}
				new GetTxnID().execute(url);
			}

		}

		@Override
		public void onFormResubmission(WebView view, Message dontResend,
				Message resend) {
			// TODO Auto-generated method stub
			resend.sendToTarget();
		}

		@Override
		public void onReceivedSslError(WebView view, SslErrorHandler handler,
				SslError error) {
			// TODO Auto-generated method stub

			handler.proceed(); // Ignore SSL certificate errors
		}

	}

	private class GetTxnIDManually extends AsyncTask<String, Void, Boolean> {
		
		HashMap<String, Object> map;
//		JsonObject jObject = null;
		String resultReturn;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			// TODO Auto-generated method stub
			try {
				resultReturn = executeTxnIdManually();
			//	System.out.println("the result of d7 is "+resultReturn);
				JsonParser parser = new JsonParser();
				
				// System.out.println("the response is  / "+ response);
				if (!resultReturn.equals("")) {
					map = Utils.getInstance().jsonToMap(new JSONObject(resultReturn));
//					jObject = (JsonObject) parser.parse(resultReturn);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (result) {
				if (map != null) {
					
					try
					{
					bundle = new Bundle();
					for (Map.Entry<String, Object> entry : map.entrySet()) {
						bundle.putString(entry.getKey(), entry.getValue().toString());
					}
					bundle.putString(MerchantInfo.BILL_NAME, "" + bill_name);
					bundle.putString(MerchantInfo.BILL_EMAIL, "" + bill_email);
					bundle.putString(MerchantInfo.BILL_MOBILE, "" + bill_mobile);
					bundle.putString(MerchantInfo.BILL_DESC, "" +bill_desc);
					bundle.putString(MerchantInfo.CHANNEL, "" + Channel);
					bundle.putString(MerchantInfo.CURRENCY, "" +Currency);
					bundle.putString(MerchantInfo.ORDER_ID, "" + OrderId);
					li.removeAllViews();

					try {
						((MOLPayActivity) getActivity()).onFinishData(bundle);
					} catch (Exception e) {
						Log.i("webview", "" + e);
					}
					}
					catch(Exception Ex)
					{
						((MOLPayActivity) getActivity()).onFinishData(null);
					}
				} else {
					((MOLPayActivity) getActivity()).onFinishData(null);
				}
			}
		}
	}

	private class GetTxnID extends AsyncTask<String, Void, Boolean> {

		JsonObject jArray = null;
		String url;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// dialog.setMessage("Loading Payment Information...");
			// dialog.show();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			// TODO Auto-generated method stub
			try {
				url = params[0];
				String response = executeHttpGet(url);
				JsonParser parser = new JsonParser();
			 
				if (!response.equals("")) {

					jArray = (JsonObject) parser.parse(response);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (result) {
				if (jArray != null) {
					transactionId = parseJsonObject(jArray);
					new GetPaymentInfo().execute();
				}
			}
		}
	}
	private String parseJsonObject(JsonObject json) {

		String txn_id;

		txn_id = json.get("tranID").getAsString();

		return txn_id;
	}

	private class GetPaymentInfo extends AsyncTask<Void, Void, Boolean> {
		
		HashMap<String, Object> map;
		//JsonObject jArray = null;

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String result = executeHttpPost();
			//JsonParser parser = new JsonParser();
			if (!(result != null && result.equals(""))) {
				try {
					JSONObject jsonObject = new JSONObject(result);
					cashingIfCashChannel(getActivity(), jsonObject);
					map = Utils.getInstance().jsonToMap(jsonObject);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//jArray = (JsonObject) parser.parse(result);
			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (result) {

				if (map != null) {
			try
			{
					bundle = new Bundle();
					for (Map.Entry<String, Object> entry : map.entrySet()) {
						if (entry.getValue() instanceof List)
							bundle.putStringArrayList(entry.getKey(), new ArrayList<String>((List)entry.getValue()));
						else
							bundle.putString(entry.getKey(), entry.getValue().toString());
					}
					
					bundle.putString(MerchantInfo.BILL_NAME, "" + bill_name);
					bundle.putString(MerchantInfo.BILL_EMAIL, "" + bill_email);
					bundle.putString(MerchantInfo.BILL_MOBILE, "" + bill_mobile);
					bundle.putString(MerchantInfo.BILL_DESC, "" +bill_desc);
					bundle.putString(MerchantInfo.CHANNEL, "" + Channel);
					bundle.putString(MerchantInfo.CURRENCY, "" +Currency);
					bundle.putString(MerchantInfo.ORDER_ID, "" + OrderId);
					li.removeAllViews();

//					int status = Integer.parseInt(data[2]);
					String status = map.get("status_code").toString().trim();
					if (Channel.equals("Cash-711") || Channel.equals("Cash-PermataBank") || Channel.equals("Cash-epay")){
                        ReceiptFragment receiptFragment = ReceiptFragment.newInstance(bundle);
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(Utils.getInstance().getResource(getActivity(), "frag", ResourceType.id), receiptFragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    } else if (status.equals(22)) {
						pendingAlert.show();
					} else {

						try {
							((MOLPayActivity) getActivity())
									.onFinishData(bundle);
						} catch (Exception e) {
							Log.i("webview", "" + e);
						}
					}
			}
			catch(Exception Ex)
			{
				//Log.e(TAG_LOG, Ex.toString());
				((MOLPayActivity) getActivity()).onFinishData(null);
			}

				} else {
					((MOLPayActivity) getActivity()).onFinishData(null);
				}
			}
		}
	}

	private String[] parseJsonObjectFromPaymentResult(JsonObject json) {

		String txn_id;
		int paydate;
		String amount;
		String status_code;
		String err_desc;
		String app_code;

		txn_id = json.get("txn_ID").getAsString();
		paydate = json.get("paydate").getAsInt();

		amount = json.get("amount").getAsString();

		status_code = json.get("status_code").getAsString();

		err_desc = json.get("err_desc").getAsString();
		// err_desc="error";
		app_code = json.get("app_code").getAsString();

		String[] data = { txn_id, amount, status_code, err_desc, app_code,String.valueOf(paydate)};

		return data;
	}

	public String executeHttpGet(String URL) throws Exception {
		BufferedReader in = null;
		try {
			HttpClient client = new DefaultHttpClient();
			client.getParams().setParameter(CoreProtocolPNames.USER_AGENT,
					"android");
			HttpGet request = new HttpGet();
			request.setHeader("Content-Type", "application/json; charset=utf-8");
			request.setURI(new URI(URL));
			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));

			StringBuffer sb = new StringBuffer("");
			String line = "";

			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();
			String page = sb.toString();

			return page;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					Log.d("BBB", e.toString());
				}
			}
		}
	}

	private String executeTxnIdManually() {
		String result = "";

		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); // Timeout
		// Limit
		HttpResponse response;
		JsonObject json = new JsonObject();
		try {
			String combinationMsg = MerchantId + "D7" + OrderId
						+ Float.toString(Amount) + VerifyKey;
			String vcode = Utils.getInstance().string2MD5(combinationMsg);

			URI url = new URI(request);

			HttpPost post = new HttpPost(url);
			json.addProperty("merchant_id", MerchantId);
			json.addProperty("order_ID", OrderId);
			json.addProperty("amount", Float.toString(Amount));
			json.addProperty("chksum", vcode);
			json.addProperty("msgType", "D7");

			 Log.i("order ID", MerchantId+" "+ OrderId+" "+Amount+" "+vcode+
			 "json "+json.toString());

			post.setHeader("Content-type", "application/json");

			StringEntity se = new StringEntity(json.toString());
			se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			post.setEntity(se);
			response = client.execute(post);

			if (response != null) {
				InputStream in = response.getEntity().getContent();
				result = Utils.getInstance().convertStreamToString(in);
			}

			post.abort();
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private String executeHttpPost() {
		String result = "";

		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); // Timeout
		// Limit
		HttpResponse response;
		JsonObject json = new JsonObject();
		try {
			String combinationMsg = MerchantId + "C5" + transactionId
					+ Float.toString(Amount) + VerifyKey;
			String vcode = Utils.getInstance().string2MD5(combinationMsg);

			URI url = new URI(request);

			HttpPost post = new HttpPost(url);
			json.addProperty("merchant_id", MerchantId);
			json.addProperty("txn_ID", transactionId);
			json.addProperty("amount", Float.toString(Amount));
			json.addProperty("chksum", vcode);
			json.addProperty("msgType", "C5");

			post.setHeader("Content-type", "application/json");

			StringEntity se = new StringEntity(json.toString());
			se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			post.setEntity(se);
			response = client.execute(post);

			if (response != null) {
				InputStream in = response.getEntity().getContent();
				result = Utils.getInstance().convertStreamToString(in);
                in.close();
			}

			post.abort();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
            return result;
        }
    }

	@Override
	public void onDestroy() {
		super.onDestroy();
		// alert.dismiss();
		if (alert != null && alert.isShowing()) {
			alert.cancel();
		}
	}
	protected AlertDialog exitDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(
				"Pressing the back button will close the payment page , Are you sure you want to proceed? ")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								click = 1;
								alert.dismiss();
								new GetTxnIDManually().execute();
								 
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.dismiss();

					}
				});

		return alert = builder.create();
		// if (!alert.isShowing() && click == 0) {
		// alert.show();
		// }
	}
	 
	 

	private void DialogExit() {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		builder.setTitle("Thank you");
		builder.setMessage("Your payment request has been successfully recorded.");
		builder.setCancelable(false);
		builder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int arg1) {
				// TODO Auto-generated method stub

				pendingAlert.cancel();
				((MOLPayActivity) getActivity()).onFinishData(bundle);
			}
		});
		final TextView t_view = new TextView(getActivity());
		final ScrollView s_view = new ScrollView(getActivity());
		t_view.setText("* Note : You have 48 hour to make payment. Otherwise your order will be void.");
		t_view.setTextColor(Color.RED);
		t_view.setTextSize(14);
		s_view.addView(t_view);
		builder.setView(s_view);
		pendingAlert = builder.create();

	}

	private void cashingIfCashChannel(Context ctx, JSONObject jsonObject){
		try {
			Channel = jsonObject.get(MerchantInfo.CHANNEL).toString();
			if (Channel.equals("Cash-711") || Channel.equals("Cash-PermataBank") || Channel.equals("Cash-epay")) {
				System.out.println("Storing data...");
				SecurePreferences preferences =  new SecurePreferences(ctx, "cash", SECRET_KEY, true);
				preferences.put(jsonObject.getString(MerchantInfo.TXN_ID), jsonObject.toString());
				preferences.put(jsonObject.getString(MerchantInfo.ORDER_ID), jsonObject.getString(MerchantInfo.TXN_ID));
				System.out.println("Storing data... done");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

}