package com.molpay.molpaylib;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.molpay.molpaylib.utilities.CustomTimer;
import com.molpay.molpaylib.utilities.Utils;
import com.molpay.molpaylib.utilities.Utils.ResourceType;

import java.util.ArrayList;
import java.util.List;

public class PaymentCCFrag extends Fragment {

	// OrderInfo
	private String OrderId;
	private String Channel;
	private String Currency;
	private float Amount;
	private String cvv;

	private TextView txt_amt, txt_countryCode, txt_orderId, txt_bankName,txt_expdate;
	private Spinner edt_cardNo;
	private ImageView cardType;
	// private TextView edt_bankName;
	private Button btn_pay;
	private EditText edt_cvv;
	int type;
	private ScrollView scroll;
	private TextView txt_currency;
	
	// private String[] bank = null;
	private JSONArray jArray;
	private JSONObject jOSelect;
	private Bundle extras;
	private LinearLayout li_addcc;
	Boolean showCCDetails = false;
	View v, bankNameContainer, View04;
	String cardTs;

	String token="";
	int selected;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		extras = this.getArguments().getBundle("bundle");
		String js = this.getArguments().getString("cclist");

		try {
			JSONObject jobj = new JSONObject(js);
			jArray = jobj.getJSONArray("result");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// // .getString("message");

		if (extras != null) {
			OrderId = extras.getString("OrderId");
			Amount = extras.getFloat("Amount");
			Channel = extras.getString("Channel");
			Currency = extras.getString("Currency");
			token=extras.getString("token");
			showCCDetails = extras.containsKey("ShowCCDetails") ? extras.getBoolean("ShowCCDetails") : false;
		}

		try {
			v = inflater.inflate(Utils.getInstance().getResource(getActivity(), "payment_cc_", ResourceType.layout), container, false);

			bankNameContainer = v.findViewById(Utils.getInstance().getResource(getActivity(), "bank_name_container", ResourceType.id));
			View04 = v.findViewById(Utils.getInstance().getResource(getActivity(), "View04", ResourceType.id));
			txt_orderId = (TextView) v.findViewById(Utils.getInstance().getResource(getActivity(), "txt_orderId", ResourceType.id));
			txt_bankName = (TextView) v.findViewById(Utils.getInstance().getResource(getActivity(), "txt_bankName", ResourceType.id));
			txt_currency = (TextView) v.findViewById(Utils.getInstance().getResource(getActivity(), "txt_currency", ResourceType.id));
			txt_amt = (TextView) v.findViewById(Utils.getInstance().getResource(getActivity(), "txt_amt", ResourceType.id));
			edt_cardNo = (Spinner) v.findViewById(Utils.getInstance().getResource(getActivity(), "edt_cardNo", ResourceType.id));
			cardType = (ImageView) v.findViewById(Utils.getInstance().getResource(getActivity(), "img_cardType", ResourceType.id));
			scroll = (ScrollView) v.findViewById(Utils.getInstance().getResource(getActivity(), "scrollView1", ResourceType.id));

			String amt = null;
			if(Currency.equals("IDR"))
			{
				amt=JSONnString.toRupiahFormat(String.valueOf(Amount),false);
				txt_amt.setTextSize(35);
			}
			else
			{
				
			
			amt = JSONnString.printAmount(Amount);
			}
			 
			txt_amt.setText("" + amt);
			txt_countryCode = (TextView) v.findViewById(Utils.getInstance().getResource(getActivity(), "txt_countryCode", ResourceType.id));
			txt_expdate = (TextView) v.findViewById(Utils.getInstance().getResource(getActivity(), "txt_expdate", ResourceType.id));
			btn_pay = (Button) v.findViewById(Utils.getInstance().getResource(getActivity(), "btn_pay", ResourceType.id));
			edt_cvv = (EditText) v.findViewById(Utils.getInstance().getResource(getActivity(), "edt_cvv", ResourceType.id));
			li_addcc = (LinearLayout) v.findViewById(Utils.getInstance().getResource(getActivity(), "li_addcc", ResourceType.id));
			
			if(!showCCDetails){
				bankNameContainer.setVisibility(View.GONE);
				View04.setVisibility(View.GONE);
			} else{
				bankNameContainer.setVisibility(View.VISIBLE);
				View04.setVisibility(View.VISIBLE);
			}
		} catch (Exception e) {

		}

		
		txt_currency.setText(Currency);
		txt_orderId.setText(OrderId);

		JSONObject jO = null;
		String cardX;

		List<String> list;
		try {
			list = new ArrayList<String>();

			for (int yy = 0; yy < jArray.length(); yy++) {
				jO = jArray.getJSONObject(yy);
            if(jO.getString("token").equals(token))
            		{
            	   selected=yy;
            		}
				cardTs = jO.getString("bin");
				String first4 = cardTs.substring(0, 4);
				String second2 = cardTs.substring(4, 6);

				cardX = first4 + " " + second2 + "** **** "
						+ jO.getString("bin4");
				list.add(cardX);
			}


			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
					getActivity(), Utils.getInstance().getResource(getActivity(), "list_item", ResourceType.layout), list);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			edt_cardNo.setAdapter(adapter);
			edt_cardNo.setSelection(selected);
			edt_cardNo.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int select, long arg3) {
					// TODO Auto-generated method stub

					try {
						jOSelect = jArray.getJSONObject(select);
						String bank = jOSelect.getString("issuer_bank");
						String country = jOSelect.getString("bank_country");
						if( !bank.equals("")){
							txt_bankName.setText(bank);
						} 
						
						if(!bank.equals("nul")){
							txt_countryCode.setText(country);
						}
						
						cardTs = jOSelect.getString("bin");

						String mmyy = jOSelect.getString("expdate");
						String yy = mmyy.substring(0, 2);
						String mm = mmyy.substring(2, 4);
						txt_expdate.setText(mm + "/" + yy);

						String cardT = cardTs.substring(0, 1);
						type = Integer.parseInt(cardT);

						if (type == 4) {
							cardType.setImageResource(Utils.getInstance().getResource(getActivity(), "visa", ResourceType.drawable));
						} else if (type == 5) {
							cardType.setImageResource(Utils.getInstance().getResource(getActivity(), "mastercard", ResourceType.drawable));
						} else {
							cardType.setImageResource(Utils.getInstance().getResource(getActivity(), "ccard", ResourceType.drawable));
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	//	btn_pay.setEnabled(false);

	/*edt_cvv.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.length() == 3) {
					btn_pay.setEnabled(true);
				} else if (s.length() < 3) {
					btn_pay.setEnabled(false);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
				scroll.invalidate();
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}
		});*/

		btn_pay.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				cvv = edt_cvv.getText().toString();
				if(!cvv.equals(""))
				{
				jArray = new JSONArray();
				jArray.put(jOSelect);			

				new PayNow().execute();
				}
				else
				{
					String errorText = "Please input your cvv";
					errorDialog(errorText);
				}
			}
		});

		li_addcc.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				PaymentCCNoTokenFrag paymentCCNoTokenFrag = new PaymentCCNoTokenFrag();

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();

				Bundle bundle = new Bundle();
				bundle.putBundle("bundle", extras);
				paymentCCNoTokenFrag.setArguments(bundle);
				transaction.add(Utils.getInstance().getResource(getActivity(), "frag", ResourceType.id), paymentCCNoTokenFrag);
				transaction.addToBackStack(null);
				transaction.commit();

			}
		});

//		v.setFocusableInTouchMode(true); // ZooL - Back button - 14/2/14
//		v.requestFocus();
//		v.setOnKeyListener(new View.OnKeyListener() {
//			@Override
//			public boolean onKey(View v, int keyCode, KeyEvent event) {
//
//				switch (keyCode) {
//				case KeyEvent.KEYCODE_BACK:
//
//					return true;
//				}
//				return false;
//
//			}
//		});

		return v;
	}

	public void backButtonClick()
	{
		if (Channel.equalsIgnoreCase("list_card")) {
			((MOLPayActivity) getActivity()).onFinishData(null);
		} else {
			getFragmentManager().popBackStack();
		}
	}

	private class PayNow extends AsyncTask<Void, Void, Boolean> {

		// JsonObject jArray = null;

		String HTMLResult;

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			HTMLResult = JSONnString
					.paymentHttp(extras, jArray.toString(), cvv);
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (result) {
				if(Utils.getInstance().isJSONValid(HTMLResult))
				{
					String errMsg="";
					String errcode="";
					try
					{
						JSONObject json=new JSONObject(HTMLResult);
						errcode=json.getString("error_code");
						errMsg=json.getString("error_message");

					}
					catch(Exception Ex)
					{
						errcode="Unexpected error";
						errMsg="There was unexpected error while proceeding with payment .";

					}
					Utils.getInstance().errorDontDialogExit(getActivity(), "Error ("+errcode+")", errMsg);
				} else{
					PaymentWebViewFrag webviewFrag = new PaymentWebViewFrag();

					FragmentTransaction transaction = getFragmentManager()
							.beginTransaction();

					Bundle bundle = new Bundle();
					//extras.putString("Channel", "credit");
					bundle.putBundle("bundle", extras);
					bundle.putString("web", HTMLResult);
					webviewFrag.setArguments(bundle);
					transaction.replace(Utils.getInstance().getResource(getActivity(), "frag", ResourceType.id), webviewFrag);
					transaction.addToBackStack(null);
					transaction.commit();
				}
			}
		}
	}
	private void errorDialog(String ErrorText) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Oops");
		builder.setMessage("" + ErrorText)
				.setCancelable(true)
				.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						});

		AlertDialog alert2 = builder.create();
		alert2.show();
	}
}
