package com.molpay.molpaylib;


import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.molpay.molpaylib.settings.MerchantInfo;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EnumMap;
import java.util.Map;

import com.molpay.molpaylib.utilities.Utils;
import com.molpay.molpaylib.utilities.Utils.ResourceType;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ReceiptFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReceiptFragment extends Fragment {
    private static final String BARCODE_LINK = "https://onlinepayment.com.my/MOLPay/barcode_frankie.php?auth=molpayKEY001&barcode=";
    String transactionId, verificationCode, payableAmount, currency, expiredAt, desciption, textView2Text;
    private DisplayImageOptions options;
    private AlertDialog alert;
    private ImageView transactionBarcodePreview;
    private ImageView verificationBarcodePreview;
    private String channel;
//    EAN13VIew transactionBarcodePreview, verificationBarcodePreview;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment ReceiptFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ReceiptFragment newInstance(Bundle args) {
        ReceiptFragment fragment = new ReceiptFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public ReceiptFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            if (getArguments() != null) {
                transactionId = getArguments().getString(MerchantInfo.TXN_ID);
                verificationCode = getArguments().getString(MerchantInfo.P_CODE);
                payableAmount = getArguments().getString(MerchantInfo.PAYABLE_AMOUNT);
                currency = getArguments().getString(MerchantInfo.CUR);
                if (getArguments().getStringArrayList(MerchantInfo.NOTES) != null &&
                        getArguments().getStringArrayList(MerchantInfo.NOTES).size() > 1) {
                    desciption = getArguments().getStringArrayList(MerchantInfo.NOTES).get(0);
                    expiredAt = getArguments().getStringArrayList(MerchantInfo.NOTES).get(1);
                } else {
                    expiredAt = getArguments().getString(MerchantInfo.EXPIRED_AT);
                }
                textView2Text = getArguments().getString(MerchantInfo.PCODE_LABLE);
                channel =  getArguments().getString(MerchantInfo.CHANNEL);
            }
            options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).considerExifParams(true)
                    .bitmapConfig(Bitmap.Config.RGB_565).build();
        }catch (Exception ex)
        {
            getActivity().finish();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = null;
        if (channel.equals("Cash-PermataBank")){
            view = inflater.inflate(Utils.getInstance().getResource(getActivity(), "fragment_receipt_two", ResourceType.layout), container, false);
        } else if ("Cash-epay".equals(channel)){
            view = inflater.inflate(Utils.getInstance().getResource(getActivity(), "fragment_receipt_epay", ResourceType.layout), container, false);
        } else {
            view = inflater.inflate(Utils.getInstance().getResource(getActivity(), "fragment_receipt", ResourceType.layout), container, false);
            transactionBarcodePreview = (ImageView)view.findViewById(Utils.getInstance().getResource(getActivity(), "transaction_barcode_preview", ResourceType.id));
            verificationBarcodePreview = (ImageView)view.findViewById(Utils.getInstance().getResource(getActivity(), "verification_barcode_preview", ResourceType.id));
        }

        TextView payableAmountTextView = (TextView)view.findViewById(Utils.getInstance().getResource(getActivity(), "payable_amount", ResourceType.id));
        TextView transactionText = (TextView)view.findViewById(Utils.getInstance().getResource(getActivity(), "transaction_barcode_text", ResourceType.id));
        TextView verificationText = (TextView)view.findViewById(Utils.getInstance().getResource(getActivity(), "verification_barcode_text", ResourceType.id));
        TextView expiredAtText = (TextView)view.findViewById(Utils.getInstance().getResource(getActivity(), "paydate_textview", ResourceType.id));
        TextView desText = (TextView)view.findViewById(Utils.getInstance().getResource(getActivity(), "des_textview", ResourceType.id));
        TextView textView2 = (TextView)view.findViewById(Utils.getInstance().getResource(getActivity(), "textView2", ResourceType.id));

        try {
            payableAmountTextView.setText(String.format("%s%s*", currency, payableAmount));
            if (desciption == null) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date date = dateFormat.parse(expiredAt);
                expiredAtText.setText(String.format("** Please pay before %s", dateFormat.format(date)));
            } else {
                desText.setText(desciption);
                expiredAtText.setText(expiredAt);
            }

            transactionText.setText(transactionId);

            if (channel.equals("Cash-711")){
                if (transactionBarcodePreview != null && verificationBarcodePreview != null){
                    transactionBarcodePreview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            transactionBarcodePreview.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            int width = transactionBarcodePreview.getMeasuredWidth();
                            int height = transactionBarcodePreview.getMeasuredHeight();

                            Bitmap transactionIdBitmap = null;
                            try {
                                transactionIdBitmap = encodeAsBitmap(transactionId, BarcodeFormat.EAN_13, width - 100, height - 10);
                            } catch (WriterException e) {
                                e.printStackTrace();
                            } catch (FormatException e) {
                                e.printStackTrace();
                            }
                            transactionBarcodePreview.setImageBitmap(transactionIdBitmap);
                        }
                    });

                    verificationBarcodePreview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            verificationBarcodePreview.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            int width = verificationBarcodePreview.getMeasuredWidth();
                            int height = verificationBarcodePreview.getMeasuredHeight();

                            Bitmap verifyCodeBitmap = null;
                            try {
                                verifyCodeBitmap = encodeAsBitmap(verificationCode, BarcodeFormat.EAN_13, width - 100, height - 10);
                            } catch (WriterException e) {
                                e.printStackTrace();
                            } catch (FormatException e) {
                                e.printStackTrace();
                            }
                            verificationBarcodePreview.setImageBitmap(verifyCodeBitmap);
                        }
                    });
                }
            }

            if (verificationText != null && verificationCode != null && !verificationCode.isEmpty()) {
                verificationText.setText(verificationCode);
                textView2.setText(textView2Text);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }



    public boolean backPressed() {
        if (getActivity() != null && getArguments() != null)
        ((MOLPayActivity)getActivity()).onFinishData(getArguments());
        return true;
    }

    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;

    Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException, FormatException {
        while (contents.length() < 12) contents = "0" + contents;
        contents = generateEANContentChecksum(contents);
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            System.out.println("Encode"+ iae.toString());
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }

    String generateEANContentChecksum(CharSequence s) throws FormatException {
        int length = s.length();
        if (length == 0) {
            return "";
        }

        if (s.length() != 12)
            throw new IllegalArgumentException();

        s = s + "0";
        length = s.length();

        int sum = 0;
        for (int i = length - 2; i >= 0; i -= 2) {
            int digit = (int) s.charAt(i) - (int) '0';
            if (digit < 0 || digit > 9) {
                throw FormatException.getFormatInstance();
            }
            sum += digit;
        }
        sum *= 3;
        for (int i = length - 1; i >= 0; i -= 2) {
            int digit = (int) s.charAt(i) - (int) '0';
            if (digit < 0 || digit > 9) {
                throw FormatException.getFormatInstance();
            }
            sum += digit;
        }
        int remain = sum % 10;
        if (remain > 0)
            s = s.subSequence(0, s.length() - 1) + "" + (10 - remain);

        return s.toString();
    }
}