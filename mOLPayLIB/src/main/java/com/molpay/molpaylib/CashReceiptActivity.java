package com.molpay.molpaylib;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.molpay.molpaylib.settings.MerchantInfo;
import com.molpay.molpaylib.utilities.SecurePreferences;
import com.molpay.molpaylib.utilities.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CashReceiptActivity extends FragmentActivity {

    private static final String SECRET_KEY = "e1f5b6d6f0f1bc13d70029da4284c1be018c6151";
    ProgressBar progressBar;
    private Bundle bundle;
    private ProgressBar pgBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar mActionBar = getActionBar();

        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View mCustomView = inflator.inflate(Utils.getInstance().getResource(this, "action_bar_layout", Utils.ResourceType.layout), null);
        ImageButton imageButton = (ImageButton) mCustomView
                .findViewById(Utils.getInstance().getResource(this, "btn_back", Utils.ResourceType.id));
        imageButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayShowCustomEnabled(true);
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT);
        mActionBar.setCustomView(mCustomView, layoutParams);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                this.getApplicationContext()).build();
        ImageLoader.getInstance().init(config);
        setContentView(R.layout.activity_cash_receipt);
        progressBar = (ProgressBar)findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            String merchantId = bundle.containsKey("MerchantId") ? bundle.getString("MerchantId").trim() : "";
            String transactionId = bundle.containsKey("TransactionId") ? bundle.getString("TransactionId").trim() : "" ;
            String orderId = bundle.containsKey("OrderId") ? bundle.getString("OrderId").trim() : "" ;
            float amount  = bundle.containsKey("Amount") ? bundle.getFloat("Amount") : 0 ;
            String verifyKey = bundle.containsKey("VerifyKey") ? bundle.getString("VerifyKey").trim() : "";
            String[] params = new String[]{merchantId, transactionId, orderId, "" + amount, verifyKey};

            if(Utils.getInstance().isNetworkConnected(this)){
                new RequestResult().execute(params);
            } else {
                SecurePreferences preferences =  new SecurePreferences(this, "cash", SECRET_KEY, true);
                try {
                    if(transactionId.equals("") && !orderId.equals(""))
                        transactionId = preferences.getString(orderId);
                    String jsonStr = preferences.getString(transactionId);
                    startReceiptFragment(jsonStr);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void finish() {
        if (bundle != null) {
            bundle = filter(bundle);
            Intent data = new Intent();
            data.putExtra("bundle", bundle);
            setResult(RESULT_OK, data);
        }
        super.finish();
    }

    Bundle filter(Bundle bundle){
        if (bundle.containsKey("notes")) bundle.remove("notes");
        if (bundle.containsKey("chksum")) bundle.remove("chksum");
        if (bundle.containsKey("msgType")) bundle.remove("msgType");
        return bundle;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_cash_receipt, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void startReceiptFragment(String result) throws JSONException {
        progressBar.setVisibility(View.GONE);
        JSONObject jsonObj = Utils.getInstance().filterJsonObject(new JSONObject(result));
        if (jsonObj.getString(MerchantInfo.STATUS_CODE).equals("22")){
            HashMap<String, Object> map = Utils.getInstance().jsonToMap(jsonObj);
            bundle = new Bundle();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                if (entry.getValue() instanceof List)
                    bundle.putStringArrayList(entry.getKey(), new ArrayList<String>((List)entry.getValue()));
                else
                    bundle.putString(entry.getKey(), entry.getValue().toString());
            }
            ReceiptFragment fragInfo = ReceiptFragment.newInstance(bundle);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragInfo);
            fragmentTransaction.commit();
        } else {
            finish();
        }
    }

    private class RequestResult extends AsyncTask<String, Void, Boolean>
    {
        String HTMLResult;
        @Override
        protected Boolean doInBackground(String... param) {
            String merchant_id = ""+param[0];
            String txn_id = ""+param[1];
            String order_id = "" + param[2];
            float amount = Float.parseFloat(param[3]);
            String verifyKey =""+param[4];
            if (txn_id != "") {
                HTMLResult = JSONnString.requestResult(merchant_id, txn_id, amount,verifyKey, true);
            } else if(order_id != "")
            {
                HTMLResult = JSONnString.GetTxnIdManually(merchant_id, order_id, amount, verifyKey);
            }
            return true;
        }
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result) {
                try {
                    startReceiptFragment(HTMLResult);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
