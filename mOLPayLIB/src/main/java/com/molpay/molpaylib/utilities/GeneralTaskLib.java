package com.molpay.molpaylib.utilities;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

 





import java.util.Iterator;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


 


 

 





import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.util.Log;

public class GeneralTaskLib extends AsyncTask<String, String, Void> {
	public AsyncResponseLib delegate=null;
	
	ArrayList<String>labels=new ArrayList<String>();
	String Func;
	Context ctx;
	   InputStream is = null ;
	    String result = "";
	    Boolean prog;
	    String key;
	    private ProgressDialog progressDialog;
	    String device;
	public GeneralTaskLib(ArrayList<String>Labels,String fun,Context c,Boolean prog,String key,String device)
	{
	
		this.labels=Labels;
		this.Func=fun;
		this.ctx=c;
		this.prog=prog;
		this.key=key;
		this.device=device;
	 
		if(prog)
		{
		progressDialog = new ProgressDialog(ctx);
		}
	}
	@Override
	protected Void doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		String url_select = "https://web.molwallet.com/MOLWallet-RC/index.php?mod=API&opt="+Func;
	 
		
		
		HttpClient httpClient = getNewHttpClient();
		HttpPost httpPost = new HttpPost(url_select);
		httpPost.setHeader("Accept", "application/json");

		UsernamePasswordCredentials creds = new UsernamePasswordCredentials(
				"molwalletapi", "M0LW@llet@p1");
			 
	
		 
		try {
			   httpPost.addHeader(new BasicScheme().authenticate(creds, httpPost));
			  } catch (AuthenticationException e1) {
			   // TODO Auto-generated catch block
			   e1.printStackTrace();
			  }
	
		ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
		//param.add(new BasicNameValuePair("user", userId));
		String pass="";
		String check="";
         pass=sha1Hash("2b9efb4ad7bbcae6f3a859d49de1137d");







JSONObject json = new JSONObject();
 
 
 
try {
for(int i=0;i<labels.size();i++)
{
	if(labels.get(i).equals("conditionvalues"))	{
		 
		
		json.put(labels.get(i),new JSONObject(params[i]));
	}
	else if(labels.get(i).equals("attributes"))
	{
	 try
	 {
		 JSONObject attrs = new JSONObject(params[i]);
		 json.put(labels.get(i),attrs);
	 }
	 catch(Exception Ex)
	 {
		 
	 }
	}
	else if(labels.get(i).equals("invoice_detail"))
	{
	 try
	 {
		  JSONArray array = new JSONArray(params[i]);
		   json.put(labels.get(i),array);
	 }
	 catch(Exception Ex)
	 {
		 
	 }
	}
	else
	{
    json.put(labels.get(i),params[i]);
	}
}

} catch (JSONException e1) {
// TODO Auto-generated catch block
e1.printStackTrace();
}

 System.out.println("the params are "+json.toString());
check= sha1Hash(jsonIterator(json)+pass);
param.add(new BasicNameValuePair("Checksum", check));
System.out.println("the Checksum param is "+ check);
param.add(new BasicNameValuePair("Params", json.toString()));
param.add(new BasicNameValuePair("user_agent","1"));
param.add(new BasicNameValuePair("user_device_id", device));
	    try {
        httpPost.setEntity(new UrlEncodedFormEntity(param));
        
  

		HttpResponse httpResponse = httpClient.execute(httpPost);
		HttpEntity httpEntity = httpResponse.getEntity();

		//read content
		is =  httpEntity.getContent();					

		} catch (Exception e) {

		Log.e("log_tag", "Error in http connection "+e.toString());
		}
	try {
	    BufferedReader br = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = "";
		while((line=br.readLine())!=null)
		{
		   sb.append(line+"\n");
		}
			is.close();
			result=sb.toString();	
	 

	} catch (Exception e) {
		// TODO: handle exception
		Log.e("log_tag", "Error converting result "+e.toString());
	}
	if(prog)
	{
		   this.progressDialog.dismiss();
	}
	
		return null;
	}

	@Override
	protected void onPostExecute(Void res) {
		// TODO Auto-generated method stub
		super.onPostExecute(res);
		delegate.processFinish(result,Func,ctx);
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		if(prog)
		{
			progressDialog.setMessage(key);
			progressDialog.setCancelable(true);
			if(!progressDialog.isShowing())
			{
            progressDialog.show();
			}
			
            progressDialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface arg0) {
					// TODO Auto-generated method stub
					GeneralTaskLib.this.cancel(true);
				}
			});
            
			 
		}
		}
	
	 public static HttpClient getNewHttpClient() {
		    try {
		        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
		        trustStore.load(null, null);

		        SSLSocketFactory sf = new MySSLSocketFactoryLib(trustStore);
		        sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

		        HttpParams params = new BasicHttpParams();
		        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		        HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

		        SchemeRegistry registry = new SchemeRegistry();
		        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		        registry.register(new Scheme("https", sf, 443));

		        ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

		        return new DefaultHttpClient(ccm, params);
		    } catch (Exception e) {
		        return new DefaultHttpClient();
		    }
		}
	 public static String sha1Hash(String toHash) {
			String hash = null;
			try {
				MessageDigest digest = MessageDigest.getInstance("SHA-1");
				byte[] bytes = toHash.getBytes("UTF-8");
				digest.update(bytes, 0, bytes.length);
				bytes = digest.digest();

				// This is ~55x faster than looping and String.formating()
				hash = bytesToHex(bytes);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			return hash;
		}
		final protected static char[] hexArray = "0123456789abcdef".toCharArray();
		public static String bytesToHex(byte[] bytes) {
			char[] hexChars = new char[bytes.length * 2];
			for (int j = 0; j < bytes.length; j++) {
				int v = bytes[j] & 0xFF;
				hexChars[j * 2] = hexArray[v >>> 4];
				hexChars[j * 2 + 1] = hexArray[v & 0x0F];
			}
			return new String(hexChars);
		}
		public static String jsonIterator(JSONObject json) {

			  Iterator<?> keys = json.keys();

			  String chksumstr = "";

			  while (keys.hasNext()) {
			   String key = (String) keys.next();
			   try {
			    chksumstr = chksumstr.concat(json.getString(key));
			   } catch (JSONException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			   }
			  }

			  return chksumstr;
			 }
	 
}
