package com.molpay.molpaylib.utilities;

import android.content.Context;

public interface AsyncResponseLib {
	void processFinish(String output,String func,Context c);
}
