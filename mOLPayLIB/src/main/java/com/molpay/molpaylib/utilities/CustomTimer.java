package com.molpay.molpaylib.utilities;

import android.os.CountDownTimer;

/**
 * Created by medetzhakupov on 6/23/15.
 */
public class CustomTimer {
    private static CountDownTimer countDownTimer;
    private static CallbackMethods callbackMethods;
    private static long duration;
    private static long interval;

    public static long getCurrentDuration(){
        return duration;
    }

    public static long getInterval(){
        return interval;
    }

    public static void setCallbackMethods(CallbackMethods callbackMethods)
    {
        CustomTimer.callbackMethods = callbackMethods;
    }

    public static void createTimer(long duration, long interval){
        CustomTimer.duration = duration;
        CustomTimer.interval = interval;

        if (countDownTimer == null){
            countDownTimer = new CountDownTimer(duration, interval) {
                @Override
                public void onTick(long l) {
                    if (callbackMethods != null){
                        callbackMethods.onTick(l);
                    }
                }

                @Override
                public void onFinish() {
                    if (callbackMethods != null){
                        callbackMethods.onFinish();
                    }
                }
            };
        }
        countDownTimer.start();
    }

    public void stopTimer()
    {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }

    public interface CallbackMethods{
        void onTick(long l);
        void onFinish();
    }
}
