package com.molpay.molpaylib;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.molpay.molpaylib.authentication.Authentication;
import com.molpay.molpaylib.settings.MerchantInfo;
import com.molpay.molpaylib.transaction.Payment;
import com.molpay.molpaylib.utilities.AsyncResponseLib;
import com.molpay.molpaylib.utilities.GeneralTaskLib;
import com.molpay.molpaylib.utilities.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;

public class PaymentDetailsFrag extends Fragment implements AsyncResponseLib {

	// OrderInfo
	private boolean secureMode;
	private String OrderId;
	private float Amount;
	private String Country;
	private String Currency;
	private String Channel;
	private String bill_name;
	private String bill_desc;
	private String bill_email;
	private String bill_mobile;
	// MerchantInfo
	private String MerchantId;
	private String VerifyKey;
	private String AppName, cCode;
	private int MOLWallet_Merchant_ID;
	// private int Filter;

	private JSONObject ChannelCode;

	ProgressDialog channelProgressDialog;
	private Button proceed, btn_more;
	private TextView txt_orderId,txt_amt;
	private EditText txt_name,txt_email,txt_mobile,txt_description;
	private TextView txt_currency;
	private int paymentType;
	private TextView payment_opt;
	private ImageView img_payment;
	Bundle extras;
	private AlertDialog alert, errorAlert;
	private ArrayList<Object> itemList;
	ArrayList<String> list;
	private PaymentChannelListAdapter pcListAdapter;
	private AlertDialog.Builder builder;
	private AlertDialog paymentChannelAlert;
	private ImageView img_bankselect;
	View v;

	DisplayImageOptions options;
	protected ImageLoader imageLoader;
	Boolean editable=false;
	public boolean pressed=false;
	int paydate;
	String[] data;
	private String request = "https://www.onlinepayment.com.my/MOLPay/API/mobile_new/index.php";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		extras = getActivity().getIntent().getExtras();

		if (extras != null) {
			secureMode = extras.getBoolean("IsSecure", false);
			VerifyKey = extras.getString("VerifyKey");
			MerchantId = extras.getString("MerchantId");
			AppName = extras.getString("AppName");
			OrderId = extras.getString("OrderId");
			bill_name = extras.getString("BillName");
			bill_desc = extras.getString("BillDesc");
			bill_mobile = extras.getString("BillMobile");
			bill_email = extras.getString("BillEmail");
			Channel = extras.getString("Channel");
			Currency = extras.getString("Currency");
			Country = extras.getString("Country");
			Amount = extras.getFloat("Amount");
			editable=extras.getBoolean("editable");
			MOLWallet_Merchant_ID=extras.getInt("MOLWallet_Merchant_Id");
			// Filter = extras.getInt("filter");
		}

		cCode = "";
		int layoutId = Utils.getInstance().getResource(getActivity(), "payment_detail", Utils.ResourceType.layout);
		v =  inflater.inflate(layoutId, container, false);

		txt_currency = (TextView) v.findViewById(Utils.getInstance().getResource(getActivity(), "txt_currency", Utils.ResourceType.id));
		txt_orderId = (TextView) v.findViewById(Utils.getInstance().getResource(getActivity(), "txt_orderId", Utils.ResourceType.id));
		txt_name = (EditText) v.findViewById(Utils.getInstance().getResource(getActivity(), "txt_name", Utils.ResourceType.id));
		txt_amt = (TextView) v.findViewById(Utils.getInstance().getResource(getActivity(), "txt_amt", Utils.ResourceType.id));
		txt_email = (EditText) v.findViewById(Utils.getInstance().getResource(getActivity(), "txt_email", Utils.ResourceType.id));
		txt_mobile = (EditText) v.findViewById(Utils.getInstance().getResource(getActivity(), "txt_mobilno", Utils.ResourceType.id));
		txt_description = (EditText) v.findViewById(Utils.getInstance().getResource(getActivity(), "txt_details", Utils.ResourceType.id));
		proceed = (Button) v.findViewById(Utils.getInstance().getResource(getActivity(), "btn_pay", Utils.ResourceType.id));
		btn_more = (Button) v.findViewById(Utils.getInstance().getResource(getActivity(), "btn_more", Utils.ResourceType.id));
		payment_opt = (TextView) v.findViewById(Utils.getInstance().getResource(getActivity(), "TextView04", Utils.ResourceType.id));
		img_payment = (ImageView) v.findViewById(Utils.getInstance().getResource(getActivity(), "img_payment", Utils.ResourceType.id));
		img_bankselect = (ImageView) v.findViewById(Utils.getInstance().getResource(getActivity(), "img_bankselect", Utils.ResourceType.id));

		txt_currency.setText(Currency);


		if(!editable)
		{
			txt_name.setEnabled(false);
			txt_email.setEnabled(false);
			txt_mobile.setEnabled(false);
			txt_description.setEnabled(false);
		}

		itemList = new ArrayList<Object>();


		new transaction().execute();
		//new authentication().execute();


		img_payment.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				paymentDialogBuilder();
			}
		});

		payment_opt.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				paymentDialogBuilder();
			}
		});

		img_bankselect.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				paymentDialogBuilder();
			}
		});

		btn_more.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				// bill_desc =
				// "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
				bill_desc=txt_description.getText().toString();
				builder.setTitle("Description");
				builder.setMessage("" + bill_desc)
						.setCancelable(true)
						.setPositiveButton("Done",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
														int id) {
										dialog.dismiss();
									}
								});

				AlertDialog alert2 = builder.create();
				alert2.show();
			}
		});

		proceed.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = getActivity().getPackageManager().getLaunchIntentForPackage("com.molwallet");
				txt_name.setError(null);
				txt_email.setError(null);
				txt_mobile.setError(null);
				bill_name=txt_name.getText().toString();
				bill_email=txt_email.getText().toString();
				bill_mobile=txt_mobile.getText().toString();
				bill_desc=txt_description.getText().toString();
				if (cCode == "") {

					AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					builder.setTitle("Oops");
					builder.setMessage(
							"Please select your payment option before you proceed")
							.setCancelable(true)
							.setPositiveButton("Ok",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											dialog.dismiss();
										}
									});

					AlertDialog alert2 = builder.create();
					alert2.show();

				} else if (cCode.equals("MOLWallet")&&intent!=null)
				{
					new PayMolwallet().execute();
				}
				else
				{
					if (bill_name.length() < 1) {
						txt_name.setError("This field is required");
//						errorDontDialogExit("Invalid input", "Bill name not found");
					} else if (bill_name.length() < 2) {
						txt_name.setError("Name is invalid");
//						errorDontDialogExit("Invalid input", "Bill name not found");
					}else if (bill_email.length() < 1) {
						txt_email.setError("This field is required");
//						errorDontDialogExit("Invalid input", "Bill email not found");
					}else if (bill_email.length() < 5) {
						txt_email.setError("Email is invalid");
//						errorDontDialogExit("Invalid input", "Bill email not found");
					}else if (bill_mobile.length() < 1) {
						txt_mobile.setError("This field is required");
//						errorDontDialogExit("Invalid input", "Bill mobile not found");
					}else if (bill_mobile.length() < 5) {
						txt_mobile.setError("Mobile is invalid");
//						errorDontDialogExit("Invalid input", "Bill mobile not found");
					} else
					{

						extras.putString("BillName", bill_name);
						extras.putString("BillDesc", bill_desc);
						extras.putString("BillMobile", bill_mobile);
						extras.putString("BillEmail", bill_email);

						if(cCode.equalsIgnoreCase("credit") || cCode.equalsIgnoreCase("crossborder")) {
							new checkToken().execute();
						} else {
							new PayBank().execute();
						}
					}
				}
			}
		});

		backButton();

		setupImageLoader();

		new paymentChannel().execute();

		return v;
	}

	private boolean checkAmount(String min) {

		float fmin = Float.parseFloat(min);
		if (Amount < fmin) {
			if(Channel.equalsIgnoreCase("multi"))
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

				builder.setTitle("Invalid Amount");
				builder.setMessage("Amount is less than min amount "+Currency+" "+ fmin);
				builder.setCancelable(false);
				builder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int arg1) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});
				AlertDialog alert = builder.create();
				alert.show();
			}
			else
			{
				errorDialogExit("Invalid Amount",
						"Amount is less than min amount "+Currency+" "+ fmin);
			}
			return false;
		}
		else
		{
			return true;
		}
	}

	private class checkToken extends AsyncTask<Void, Void, Boolean> {
		String returnResult;
		JSONObject jsonObj;

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			// String mess = getResources().getString(R.string.mess_1);
			String url = null;
			returnResult = JSONnString.token(url, extras);
			return null;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			String returnStatus = null;
			Channel = cCode;
			super.onPostExecute(result);

			try {
				jsonObj = new JSONObject(returnResult);
				returnStatus = jsonObj.getString("status");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if ("true".equalsIgnoreCase(returnStatus)) {
				PaymentCCFrag paymentCCFrag = new PaymentCCFrag();

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();

				Bundle bundle = new Bundle();

				extras.putString("Channel", cCode);
				bundle.putBundle("bundle", extras);
				bundle.putString("cclist", jsonObj.toString());
				paymentCCFrag.setArguments(bundle);
				transaction.add(Utils.getInstance().getResource(getActivity(), "frag", Utils.ResourceType.id), paymentCCFrag);
				transaction.addToBackStack(null);
				transaction.commit();
				((MOLPayActivity)getActivity()).pgBar.setVisibility(View.INVISIBLE);
			} else if ("false".equalsIgnoreCase(returnStatus)) {
				int code = 0;
				JSONObject resultFalse = new JSONObject();
				// {"status":false,"result":{"code":1003,"message":"Token not found"}}
				try {
					resultFalse = jsonObj.getJSONObject("result");

					code = resultFalse.getInt("code");

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (code == 1003 || code == 1007) {
					PaymentCCNoTokenFrag paymentCCNoTokenFrag = new PaymentCCNoTokenFrag();

					FragmentTransaction transaction = getFragmentManager()
							.beginTransaction();

					Bundle bundle = new Bundle();

					extras.putString("Channel", cCode);
					bundle.putBundle("bundle", extras);
					paymentCCNoTokenFrag.setArguments(bundle);
					transaction.add(Utils.getInstance().getResource(getActivity(), "frag", Utils.ResourceType.id), paymentCCNoTokenFrag);
					transaction.addToBackStack(null);
					transaction.commit();
					((MOLPayActivity)getActivity()).pgBar.setVisibility(View.INVISIBLE);
				} else {

					try {
						String msg = resultFalse.getString("message");

						errorDialogExit("Oops", "" + msg);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

		@Override
		protected void onPreExecute() {
			((MOLPayActivity)getActivity()).pgBar.setVisibility(View.VISIBLE);
		}
	}

	private class authentication extends AsyncTask<Void, Void, Boolean> {

		ProgressDialog progressDialog;

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			Authentication 	auth = new Authentication(MerchantId, AppName,
					VerifyKey);

			return auth.requestAuth();
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			if (result) {
				new transaction().execute();
			} else {
				errorDialogExit("Invalid Merchant",
						"Merchant verification failed");
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Authentication...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.show();
		}
	}

	private class transaction extends AsyncTask<Void, Void, Boolean> {

		private String[] appData;

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub

			Payment payment = new Payment(secureMode, OrderId, MerchantId, VerifyKey,
					AppName);
			payment.setAmount(Amount);
			payment.setBillDesc(bill_desc);
			payment.setBillName(bill_name);
			payment.setBillEmail(bill_email);
			payment.setBillMobile(bill_mobile);
			payment.setChannel(Channel);
			payment.setCountry(Country);
			payment.setCurrency(Currency);
			//appData = payment.makePayment();
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (result) {
				String amt = null;

				amt = JSONnString.printAmount(Amount);

				txt_amt.setText("" + amt);
				txt_orderId.setText("" + OrderId);
				txt_name.setText("" + bill_name);
				txt_email.setText("" + bill_email);
				txt_mobile.setText("" + bill_mobile);
				txt_description.setText("" + bill_desc);
			}
		}
	}

	private class PayBank extends AsyncTask<Void, Void, Boolean> {

		// JsonObject jArray = null;

		String HTMLResult;

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			HTMLResult = JSONnString.paymentHttp(extras, "", cCode);
			System.out.println("the result of b3 is "+HTMLResult);
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (result) {
				if(HTMLResult != null && Utils.getInstance().isJSONValid(HTMLResult))
				{
					String errMsg="";
					String errcode="";
					try
					{
						JSONObject json=new JSONObject(HTMLResult);
						errcode=json.getString("error_code");
						errMsg=json.getString("error_message");

					}
					catch(Exception Ex)
					{
						errcode="Unexpected error";
						errMsg="There was unexpected error while proceeding with payment .";

					}
					errorDontDialogExit("Oops ("+errcode+")", errMsg);
				}
				else {
					pressed = true;
					PaymentWebViewFrag webviewFrag = new PaymentWebViewFrag();
					FragmentTransaction transaction = getFragmentManager()
							.beginTransaction();
					Bundle bundle = new Bundle();
					extras.putString("Channel", cCode);
					bundle.putBundle("bundle", extras);
					bundle.putString("web", HTMLResult);
					webviewFrag.setArguments(bundle);
					transaction.add(Utils.getInstance().getResource(getActivity(), "frag", Utils.ResourceType.id), webviewFrag);
					transaction.addToBackStack(null);
					transaction.commit();
				}
			}
		}
	}
	private class PayMolwallet extends AsyncTask<Void, Void, Boolean> {

		// JsonObject jArray = null;
		JsonObject jObject = null;
		String HTMLResult;

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			HTMLResult = JSONnString.paymentMolwallet(extras, "", cCode);
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (result) {
				try {


					JsonParser parser = new JsonParser();


					if (!HTMLResult.equals("")) {
						jObject = (JsonObject) parser.parse(HTMLResult);
						data = parseMOLWalletResponse(jObject);
						paydate=Integer.valueOf(data[1]);
						request();
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}
		}
	}
	private class paymentChannel extends AsyncTask<Void, Void, Boolean> {

		JSONObject jsonObj;
		String HTMLResult;

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			HTMLResult = JSONnString.paymentChannel(extras);
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			try{

				String returnStatus = null;
				String minAmt = null;
				list = new ArrayList<String>();
				ChannelCode = new JSONObject();
				super.onPostExecute(result);
				try {
					jsonObj = new JSONObject(HTMLResult);

					returnStatus = jsonObj.getString("status");

					if ("true".equalsIgnoreCase(returnStatus)) {
						JSONArray jarray = new JSONArray();

						jarray = jsonObj.getJSONArray("result");

						if (itemList != null)
							itemList.clear();
						for (int u = 0; u < jarray.length(); u++) {
							jsonObj = jarray.getJSONObject(u);
							String channel = jsonObj.getString("title");
							String code = jsonObj.getString("maskname");
							String logoUrl = jsonObj.getString("logo_url");

							if (jsonObj.isNull("minAmt") == false) {
								minAmt = jsonObj.getString("minAmt");
							}
							else
							{
								minAmt = "0";
							}

							JSONArray jcurrency = jsonObj.getJSONArray("currency");

							ChannelCode.put(code, jcurrency);

							list.add(code);

							AddObjectToList(logoUrl, channel, code, minAmt);
						}

						if (channelProgressDialog != null && channelProgressDialog.isShowing())
						{
							channelProgressDialog.dismiss();
							paymentDialogBuilder();
						}
					} else if ("false".equalsIgnoreCase(returnStatus)) {
						JSONObject resultFalse = jsonObj.getJSONObject("result");

						String msg = resultFalse.getString("message");

						errorDialogExit("Oops", "" + msg);
					} else {
						errorDialogExit("Oops",
								"Something went wrong, try again later");
					}


				} catch (JSONException e) {
					// TODO Auto-generated catch block
					Log.i("exception", ""+e);
					errorDialogExit("Invalid Merchant",
							"Merchant Username / Password failed");
				}

				if (Channel.length() > 0 && !(Channel.equalsIgnoreCase("multi"))) {
					int chkChannel = searchChannel(Channel);
					if (chkChannel > -1) {
						// int images = ChannelImages.loadImages(getActivity()
						// .getPackageName(), Channel);
						// img_bankselect.setImageResource(images);

						paymentChannelList payment = (paymentChannelList) itemList
								.get(chkChannel);
						if (checkAmount(payment.getminA())) {
							imageLoader.displayImage(payment.getImg(),
									img_bankselect, options);
							img_bankselect.setVisibility(View.VISIBLE);
							payment_opt.setVisibility(View.GONE);

							//img_bankselect.setEnabled(false);
							//img_payment.setEnabled(false);
							cCode = Channel;
						}
					} else {
						errorDialogExit("Oops", "Channel Not Found / Currency Not Supported ");
					}
				}
			}catch (Exception e){
			}
		}

	}

	private int searchChannel(String channel) {
		for (int u = 0; u < list.size(); u++) {
			String bank = list.get(u).toString();
			if (channel != null) {
				if (channel.equals(bank)) {

					try {
						JSONArray currencyArray = ChannelCode
								.getJSONArray(channel);

						for (int cr = 0; cr < currencyArray.length(); cr++) {
							String cur = currencyArray.getString(cr);
							if (Currency.equalsIgnoreCase(cur)) {
								return u;
							}
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

		return -1;
	}

	private void backButton() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(
				"Hitting the back button will close the payment page , Are you sure you want to proceed ? ")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								((MOLPayActivity) getActivity())
										.onFinishData(null);
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

					}
				});

		alert = builder.create();

	}

	private void errorDialogExit(String title, String error) {

		builder = new AlertDialog.Builder(getActivity());

		builder.setTitle("" + title);
		builder.setMessage("" + error);
		builder.setCancelable(false);
		builder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int arg1) {
				// TODO Auto-generated method stub

				errorAlert.cancel();
				((MOLPayActivity) getActivity()).onFinishData(null);
			}
		});
		errorAlert = builder.create();
		errorAlert.show();
	}
	private void errorDontDialogExit(String title, String error) {

		builder = new AlertDialog.Builder(getActivity());

		builder.setTitle("" + title);
		builder.setMessage("" + error);
		builder.setCancelable(false);
		builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int arg1) {
				// TODO Auto-generated method stub

				errorAlert.cancel();

			}
		});
		errorAlert = builder.create();
		errorAlert.show();
	}

	private void setupImageLoader() {
		imageLoader = ImageLoader.getInstance();

		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
	}

	private void paymentDialogBuilder() {
		if (itemList.size() == 0)
		{
			channelProgressDialog = new ProgressDialog(getActivity());
			channelProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			channelProgressDialog.setMessage("Loading. Please wait...");
			channelProgressDialog.setIndeterminate(true);
			channelProgressDialog.setCanceledOnTouchOutside(false);
			try {
				channelProgressDialog.show();
			}
			catch(Exception Ex)
			{

			}
			new paymentChannel().execute();

		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle("Payment Options");
			final GridView lv = new GridView(getActivity());
			pcListAdapter = new PaymentChannelListAdapter(getActivity(), itemList);
			lv.setAdapter(pcListAdapter);
			lv.setNumColumns(GridView.AUTO_FIT);
			lv.setColumnWidth(0);
			lv.setVerticalSpacing(0);
			lv.setHorizontalSpacing(2);
			lv.setPadding(10, 10, 10, 10);

			// imageLoader.init(ImageLoaderConfiguration.createDefault(context));

			lv.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View view,
										int position, long arg3) {

					paymentChannelList payment = (paymentChannelList) itemList
							.get(position);


					if (checkAmount(payment.getminA())) {

						paymentType = position;
						payment_opt.setVisibility(View.GONE);
						cCode = payment.getCode();


						imageLoader.displayImage(payment.getImg(), img_bankselect,
								options);
						img_bankselect.setVisibility(View.VISIBLE);
					}

					paymentChannelAlert.dismiss();
				}
			});
			builder.setView(lv);
			builder.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
					});
			paymentChannelAlert = builder.create();
			paymentChannelAlert.show();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// alert.dismiss();
		if (alert != null && alert.isShowing()) {
			alert.cancel();
		}

		if (errorAlert != null && errorAlert.isShowing()) {
			errorAlert.cancel();
			errorAlert.dismiss();
		}

	}

	public void AddObjectToList(String image, String channel, String code,
								String min) {
		paymentChannelList paymentList = new paymentChannelList();
		paymentList.setimg(image);
		paymentList.setChannel(channel);
		paymentList.setCode(code);
		paymentList.setminA(min);
		itemList.add(paymentList);
	}

	public class paymentChannelList {
		String img;
		String channel;
		String code;
		String minA;
		String maxA;

		public String getImg() {
			return img;
		}

		public void setimg(String url) {
			this.img = url;
		}

		public String getChannel() {
			return channel;
		}

		public void setChannel(String ID) {
			this.channel = ID;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getminA() {
			return minA;
		}

		public void setminA(String min) {
			this.minA = min;
		}

		public String getmaxA() {
			return maxA;
		}

		public void setmaxA(String max) {
			this.maxA = max;
		}
	}
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putBoolean("pressed",pressed);

	}
	void request() {

		ArrayList<String>labels=new ArrayList<String>();
		labels.add("wallet_id");
		labels.add("merchant_id");
		labels.add("amount");
		labels.add("description");
		labels.add("Currency");
		labels.add("in_app");
		labels.add("email");
		labels.add("send_notification");
		labels.add("merchant_order_id");
		GeneralTaskLib task=new GeneralTaskLib(labels,"Payment/SendRequest",getActivity(),true,"Requesting Payment","From SDK");
		Intent intent = getActivity().getPackageManager().getLaunchIntentForPackage("com.molwallet");

		task.execute(bill_mobile,String.valueOf(MOLWallet_Merchant_ID),String.valueOf(Amount),bill_desc,Currency,"1",bill_email,"2",data[0]);


		task.delegate=this;

	}

	@Override
	public void processFinish(String output, String func, Context c) {
		// TODO Auto-generated method stub
		if(func.equals("Payment/SendRequest"))
		{
			try {
				JSONObject jResult = new JSONObject(output);
				String status=jResult.getString("status");
				if("true".equalsIgnoreCase(status))
				{

					Intent intent = getActivity().getPackageManager().getLaunchIntentForPackage("com.molwallet");
					if (intent != null)
					{
						JSONObject result=jResult.getJSONObject("result");
						String invId=String.valueOf(result.getInt("invoice_id"));

						Intent i = new Intent();
						i.setComponent(new ComponentName("com.molwallet", "transferFund.SDKInvDetails"));
						i.putExtra("id",invId);
						i.putExtra("desc",bill_desc);
						Long tsLong = System.currentTimeMillis()/1000;
						String ts = tsLong.toString();
						i.putExtra("date",getDate(paydate,"dd/MM/yyyy HH:mm:ss"));
						i.putExtra("amt",String.valueOf(Amount));
						i.putExtra("mobile",AppName);
						i.putExtra("type", 1);
						i.putExtra("datetime", paydate);

						startActivityForResult(i,1);
					}
					else
					{

						AlertDialog.Builder builder = new AlertDialog.Builder(
								getActivity());

						builder.setMessage("Request has been sent successfully , please proceed with payment using your wallet app")
								.setCancelable(false)
								.setPositiveButton("OK",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog, int id) {

											}
										});
						AlertDialog alert = builder.create();
						alert.show();
					}
				}




				else
				{

					JSONObject obj = jResult.getJSONObject("result");


					AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					if(obj != null && !obj.getString("message").equals("") && !obj.getString("message").equals("null"))
					{
						builder.setMessage(obj.getString("message"))
								.setCancelable(false)
								.setPositiveButton("OK",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog, int id) {

											}
										});
					}
					else
					{
						builder.setMessage("There was error while transfering fund")
								.setCancelable(false)
								.setPositiveButton("OK",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog, int id) {

											}
										});
					}

					AlertDialog alert = builder.create();
					alert.show();


				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public static String getDate(long milliSeconds, String dateFormat)
	{
		// Create a DateFormatter object for displaying date in specified format.
		DateFormat formatter = new SimpleDateFormat(dateFormat);

		// Create a calendar object that will convert the date and time value in milliseconds to date.
		GregorianCalendar calendar = new GregorianCalendar();
		milliSeconds=milliSeconds*1000;
		calendar.setTimeInMillis(milliSeconds);
		return formatter.format(calendar.getTime());
	}

	private String[] parseMOLWalletResponse(JsonObject json) {

		String txn_id;
		int paydate;
		String amount;
		String orderId;


		txn_id = json.get("txn_ID").getAsString();
		paydate = json.get("paydate").getAsInt();
		orderId = json.get("order_id").getAsString();
		amount = json.get("amount").getAsString();



		String[] data = {txn_id,String.valueOf(paydate), amount, orderId};

		return data;
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if(resultCode==getActivity().RESULT_OK &&requestCode == 1)
		{
			Bundle bundle = data.getExtras().getBundle("result");
			Boolean stat=bundle.getBoolean("stat",false);

			if(stat)
			{
				new GetPaymentInfo().execute();

			}
			else
			{
				String error=bundle.getString("error","Payment Failed in MOLWallet");
				AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				builder.setTitle("Oops");
				builder.setMessage(
						error)
						.setCancelable(true)
						.setPositiveButton("Ok",
								new DialogInterface.OnClickListener() {
									public void onClick(
											DialogInterface dialog, int id) {
										dialog.dismiss();
									}
								});

				AlertDialog alert2 = builder.create();
				alert2.show();

			}



		}
	}

	private class GetPaymentInfo extends AsyncTask<Void, Void, Boolean> {

		JsonObject jArray = null;

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String result = executeHttpPost();

			JsonParser parser = new JsonParser();
			if (!result.equals("")) {
				jArray = (JsonObject) parser.parse(result);
			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (result) {
				if (jArray != null) {
					try
					{
						String[] data = parseJsonObjectFromPaymentResult(jArray);
						Bundle bundle = new Bundle();
						bundle.putString(MerchantInfo.TXN_ID, "" + data[0]);
						bundle.putString(MerchantInfo.PAY_AMOUNT, "" + data[1]);
						bundle.putString(MerchantInfo.STATUS_CODE, "" + data[2]);
						bundle.putString(MerchantInfo.ERR_DESC, "" + data[3]);
						bundle.putString(MerchantInfo.APP_CODE, "" + data[4]);
						bundle.putString(MerchantInfo.PAYDATE, "" + data[5]);
						bundle.putString(MerchantInfo.BILL_NAME, "" + bill_name);
						bundle.putString(MerchantInfo.BILL_EMAIL, "" + bill_email);
						bundle.putString(MerchantInfo.BILL_MOBILE, "" + bill_mobile);
						bundle.putString(MerchantInfo.BILL_DESC, "" +bill_desc);
						bundle.putString(MerchantInfo.CHANNEL, "" + Channel);
						bundle.putString(MerchantInfo.CURRENCY, "" +Currency);
						bundle.putString(MerchantInfo.ORDER_ID, "" + OrderId);


						try {
							((MOLPayActivity) getActivity())
									.onFinishData(bundle);
						} catch (Exception e) {
							e.printStackTrace();
						}

					}
					catch(Exception Ex)
					{
						Ex.printStackTrace();
						((MOLPayActivity) getActivity()).onFinishData(null);
					}

				} else {
					((MOLPayActivity) getActivity()).onFinishData(null);
				}
			}
		}
	}

	private String executeHttpPost() {
		String result = "";

		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); // Timeout
		// Limit
		HttpResponse response;
		JsonObject json = new JsonObject();
		try {
			String combinationMsg = MerchantId + "C5" + data[0]
					+ Float.toString(Amount) + VerifyKey;
			String vcode = string2MD5(combinationMsg);

			URI url = new URI(request);

			HttpPost post = new HttpPost(url);
			json.addProperty("merchant_id", MerchantId);
			json.addProperty("txn_ID", data[0]);
			json.addProperty("amount", Float.toString(Amount));
			json.addProperty("chksum", vcode);
			json.addProperty("msgType", "C5");

			post.setHeader("Content-type", "application/json");

			StringEntity se = new StringEntity(json.toString());
			se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			post.setEntity(se);
			response = client.execute(post);

			if (response != null) {
				InputStream in = response.getEntity().getContent();
				result = convertStreamToString(in);
			}

			post.abort();

			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	private String string2MD5(String md5) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes("UTF-8"));
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
						.substring(1, 3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
		} catch (UnsupportedEncodingException ex) {
		}
		return null;
	}
	public static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
	private String[] parseJsonObjectFromPaymentResult(JsonObject json) {

		String txn_id;
		int paydate;
		String amount;
		String status_code;
		String err_desc;
		String app_code;

		txn_id = json.get("txn_ID").getAsString();
		paydate = json.get("paydate").getAsInt();

		amount = json.get("amount").getAsString();

		status_code = json.get("status_code").getAsString();

		err_desc = json.get("err_desc").getAsString();
		// err_desc="error";
		app_code = json.get("app_code").getAsString();

		String[] data = { txn_id, amount, status_code, err_desc, app_code,String.valueOf(paydate)};

		return data;
	}
}
