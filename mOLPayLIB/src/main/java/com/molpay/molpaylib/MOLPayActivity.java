package com.molpay.molpaylib;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.molpay.molpaylib.settings.MerchantInfo;
import com.molpay.molpaylib.utilities.CustomTimer;
import com.molpay.molpaylib.utilities.SecurePreferences;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import com.molpay.molpaylib.utilities.Utils;

import java.io.InputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MOLPayActivity extends FragmentActivity implements CustomTimer.CallbackMethods {

	private static final String SECRET_KEY = "e1f5b6d6f0f1bc13d70029da4284c1be018c6151";
	private String request = "https://www.onlinepayment.com.my/MOLPay/API/mobile_new/index.php";
	public View timerLayout;
	public ProgressBar pgBar;
	private TextView timerTextView;
	private boolean showTimer = false;
	private long duration = 0;
	private String timerText;
	private double Amount1;

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	 
	}

	// OrderInfo
	private String OrderId;
	private float Amount;
	private String Country;
	private String Currency;
	private String Channel;
	private String bill_name;
	private String bill_desc;
	private String bill_email;
	private String bill_mobile;
	private String token;
	// MerchantInfo
	private String MerchantId;
	private String VerifyKey;
	private String AppName, USERNAME, PASSWORD;
	public Bundle bundle, extras;

	static String no_token_frag = "add_card";
	static String token_frag = "list_card";
	private AlertDialog alert, errorAlert;
	boolean debug=false;
	boolean editable=false;
	boolean is_escrow=false;
	private int MOLWallet_Merchant_ID;
	int frag = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			setContentView(Utils.getInstance().getResource(this, "molpay_main", Utils.ResourceType.layout));
			frag= Utils.getInstance().getResource(this, "frag", Utils.ResourceType.id);
			timerLayout = findViewById(Utils.getInstance().getResource(this, "timer_layout", Utils.ResourceType.id));
			timerTextView = (TextView)findViewById(Utils.getInstance().getResource(this, "timer_text_view", Utils.ResourceType.id));

			ActionBar mActionBar = getActionBar();

			LayoutInflater inflator = (LayoutInflater) this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View mCustomView = inflator.inflate(Utils.getInstance().getResource(this, "action_bar_layout", Utils.ResourceType.layout), null);
			pgBar = (ProgressBar) mCustomView.findViewById(Utils.getInstance().getResource(this, "pgBar", Utils.ResourceType.id));
			ImageButton imageButton = (ImageButton) mCustomView
					.findViewById(Utils.getInstance().getResource(this, "btn_back", Utils.ResourceType.id));
			imageButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					onBackClick();
				}
			});

			mActionBar.setDisplayShowHomeEnabled(false);
			mActionBar.setDisplayShowTitleEnabled(false);
			mActionBar.setDisplayShowCustomEnabled(true);
			mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
			ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
					ActionBar.LayoutParams.MATCH_PARENT);
			mActionBar.setCustomView(mCustomView, layoutParams);
			deleteExpiredReceipts(this);
		} catch (Exception e) {
			Log.e("MOLPayActivity", e.toString());
		}

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				this.getApplicationContext()).build();
		ImageLoader.getInstance().init(config);
		 
		extras = getIntent().getExtras();

		if (extras != null) {			
			MerchantId = extras.containsKey("MerchantId") ? extras.getString("MerchantId").trim() : ""; 
			AppName = extras.containsKey("AppName") ? extras.getString("AppName").trim() : "" ;
			VerifyKey = extras.containsKey("VerifyKey") ? extras.getString("VerifyKey").trim() : "" ;
			OrderId = extras.containsKey("OrderId") ? extras.getString("OrderId").trim() : "";
			bill_name = extras.containsKey("BillName") ? extras.getString("BillName").trim() : "";
			bill_desc = extras.containsKey("BillDesc") ? extras.getString("BillDesc").trim() : "";
			bill_mobile = extras.containsKey("BillDesc") ?  extras.getString("BillMobile").trim(): "";
			bill_email = extras.containsKey("BillDesc") ?  extras.getString("BillEmail").trim(): "";
			Channel = extras.containsKey("Channel") ?  extras.getString("Channel").trim() : "";
			Currency = extras.containsKey("Currency") ?  extras.getString("Currency").trim(): "";
			Country = extras.containsKey("Country") ?  extras.getString("Country").trim(): "";
			Amount = extras.containsKey("Amount") ?  extras.getFloat("Amount"): 0;
			USERNAME = extras.containsKey("Username") ?  extras.getString("Username").trim(): "";
			PASSWORD = extras.containsKey("Password") ?  extras.getString("Password").trim(): "";
			debug= extras.getBoolean("debug", false);
			editable= extras.getBoolean("editable", false);
			token= extras.containsKey("token") ? extras.getString("token") : "";
			is_escrow= extras.getBoolean("is_escrow", false);
			showTimer = extras.getBoolean("ShowTimer", false);
			duration = extras.getLong("Duration", 0);
			MOLWallet_Merchant_ID= extras.containsKey("MOLWallet_Merchant_Id") ?  extras.getInt("MOLWallet_Merchant_Id") : -1;
        }
		
		if(debug && isNetworkAvailable())
		{
			new debug().execute();
		}	
		extras.putString("MerchantId", MerchantId);
		extras.putString("AppName", AppName);
		extras.putString("VerifyKey", VerifyKey);
		extras.putString("Username", USERNAME);
		extras.putString("Password", PASSWORD);
		extras.putString("OrderId", OrderId);
		extras.putString("BillName", bill_name);
		extras.putString("BillDesc", bill_desc);
		extras.putString("BillMobile", bill_mobile);
		extras.putString("BillEmail", bill_email);
		extras.putString("Channel", Channel);
		extras.putString("Currency", Currency);
		extras.putString("Country", Country);
		extras.putFloat("Amount", Amount);
		extras.putBoolean("editable",editable);
		extras.putBoolean("is_escrow",is_escrow);
		extras.putInt("MOLWallet_Merchant_ID", MOLWallet_Merchant_ID);
		//intent.putExtras(extras);
		
		if(!isNetworkAvailable())
		{
			errorDialogExit("Network Issue", "Please make sure that you are connected to the internet");
		}
		else if (Amount <= 1.0f) {
			errorDialogExit("Invalid Amount", "Amount should more than 1.00");
		} 
		else if (bill_name.length() < 1 && !editable) {
			errorDialogExit("Invalid input", "Bill name not found");
		} else if (bill_email.length() < 1 && !editable) {
			errorDialogExit("Invalid input", "Bill email not found");
		} else if (bill_mobile.length() < 1 &&!editable) {
			errorDialogExit("Invalid input", "Bill mobile not found");
		}
		else if (bill_desc.length() < 1 && !editable) {
			errorDialogExit("Invalid input", "Bill description not found");
		}else {
			if (Channel != null) {

				if (Channel.equalsIgnoreCase(no_token_frag)) {

					PaymentCCNoTokenFrag ccNotoken = new PaymentCCNoTokenFrag();
					
					Bundle bundle = new Bundle();
					
					bundle.putBundle("bundle", extras);
					ccNotoken.setArguments(bundle);

					FragmentTransaction transaction = getSupportFragmentManager()
							.beginTransaction();
					
					transaction.add(frag, ccNotoken);
					transaction.addToBackStack(null);

					transaction.commit();

				} else if (Channel.equalsIgnoreCase(token_frag)) {

					new checkToken().execute();

				} else {

					PaymentDetailsFrag paymentDetails = new PaymentDetailsFrag();

					FragmentTransaction transaction = getSupportFragmentManager()
							.beginTransaction();

					transaction.add(frag, paymentDetails);
					transaction.addToBackStack(null);

					transaction.commit();
				}
			} else {
				PaymentDetailsFrag paymentDetails = new PaymentDetailsFrag();

				FragmentTransaction transaction = getSupportFragmentManager()
						.beginTransaction();

				transaction.add(frag, paymentDetails);
				transaction.addToBackStack(null);

				transaction.commit();
			}
		}

		backButton();
		if(isAlwaysFinishActivitiesOptionEnabled())
		{
			showDeveloperOptionsScreen();
		}

		if (showTimer && duration >= 60 * 1000){
			timerLayout.setVisibility(View.VISIBLE);
			try{
				timerText = getString(Utils.getInstance().getResource(this, "timer_text",
						Utils.ResourceType.string));
			}catch (Resources.NotFoundException ex){

			}
			timerText = timerText == null ? "Please pay in" : timerText;
			initTimer(duration, 1000);
		} else{
			timerLayout.setVisibility(View.GONE);
		}
	}

	public void initTimer(long duration, long interval){
		CustomTimer.setCallbackMethods(this);
		CustomTimer.createTimer(duration, interval);
	}

 
	public void onFinishData(Bundle bndle) {
		bundle = bndle;

		finish();
	}

	public void onBackClick()
	{
		Fragment currentFragment = getSupportFragmentManager().getFragments().get(getSupportFragmentManager().getFragments().size() - 1);
		if (currentFragment instanceof PaymentDetailsFrag){
			onFinishData(null);
		} else if(currentFragment instanceof  PaymentCCFrag){
			((PaymentCCFrag)currentFragment).backButtonClick();
		} else if(currentFragment instanceof  PaymentCCNoTokenFrag){
			if (Channel.equalsIgnoreCase("list_card")
					|| Channel.equalsIgnoreCase("add_card")) {
				onFinishData(null);
			} else {
				currentFragment.getFragmentManager().popBackStack();
			}

		} else if(currentFragment instanceof  PaymentWebViewFrag) {
			((PaymentWebViewFrag) currentFragment).exitDialog().show();

		} else if(currentFragment instanceof ReceiptFragment){
			if (this instanceof MOLPayActivity)
				((ReceiptFragment)currentFragment).backPressed();
			else
				onBackPressed();
		}
	}

	@Override
	public void finish() {

		if (bundle != null) {
			bundle = filter(bundle);
			Intent data = new Intent();
			data.putExtra("bundle", bundle);
			setResult(RESULT_OK, data);
		}
		super.finish();
	}


	Bundle filter(Bundle bundle){
		if (bundle.containsKey("notes")) bundle.remove("notes");
		if (bundle.containsKey("chksum")) bundle.remove("chksum");
		if (bundle.containsKey("msgType")) bundle.remove("msgType");
		return bundle;
	}

	@Override
	public void onTick(long l) {
		timerTextView.setText(
				String.format("%s %s", timerText , Utils.getInstance().convertMillsToString(l)));
	}

	@Override
	public void onFinish() {
		new GetTxnIDManually().execute();
	}

	private class checkToken extends AsyncTask<Void, Void, Boolean> {
		String returnResult;
		JSONObject jsonObj;
		ProgressDialog progressDialog;

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String url = null;
			returnResult = JSONnString.token(url, extras);

			return null;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			String returnStatus = "";
			progressDialog.dismiss();
			super.onPostExecute(result);
            //System.out.println("the token from sdk is "+returnResult);
			try {
				jsonObj = new JSONObject(returnResult);
				returnStatus = jsonObj.getString("status");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (returnStatus.equals("true")) {

				PaymentCCFrag paymentCCFrag = new PaymentCCFrag();

				FragmentTransaction transaction = getSupportFragmentManager()
						.beginTransaction();

				Bundle bundle = new Bundle();
				bundle.putBundle("bundle", extras);
				bundle.putString("cclist", jsonObj.toString());
				paymentCCFrag.setArguments(bundle);
				transaction.add(frag, paymentCCFrag);
				transaction.addToBackStack(null);
				transaction.commit();
			} else {
				PaymentCCNoTokenFrag paymentCCNoTokenFrag = new PaymentCCNoTokenFrag();

				FragmentTransaction transaction = getSupportFragmentManager()
						.beginTransaction();

				Bundle bundle = new Bundle();
				bundle.putBundle("bundle", extras);
				paymentCCNoTokenFrag.setArguments(bundle);
				transaction.add(frag, paymentCCNoTokenFrag);
				transaction.addToBackStack(null);
				transaction.commit();
			}

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(MOLPayActivity.this);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Loading...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.show();
		}
	}

	
	private class debug extends AsyncTask<Void, Void, Boolean> {
		String returnResult;
		JSONObject jsonObj;
		ProgressDialog progressDialog;

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String url = null;
		    returnResult = JSONnString.debug(url, extras);
		 
			return null;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			String returnStatus = null;
			progressDialog.dismiss();
			super.onPostExecute(result);
         
			try {
				jsonObj = new JSONObject(returnResult);
				returnStatus = jsonObj.getString("status");
				 
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			 

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(MOLPayActivity.this);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Loading...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.show();
		}
	}
	private void backButton() {
		//System.out.println("back button pressed on main page");
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(
				"Pressing the back button will close the payment page , Are you sure you want to proceed ? ")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								onFinishData(null);
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

					}
				});

		alert = builder.create();

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
        try{
            if ((keyCode == KeyEvent.KEYCODE_BACK)) {
				onBackClick();
            }
        }catch(Exception ex){
            //Log.e("MOLPayActivity", ex.toString());
        }
		return super.onKeyDown(keyCode, event);
	}

	private void errorDialogExit(String title, String error) {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				MOLPayActivity.this);

		builder.setTitle("" + title);
		builder.setMessage("" + error);
		builder.setCancelable(false);
		builder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int arg1) {
				// TODO Auto-generated method stub

				dialog.dismiss();
				finish();
			}
		});
		errorAlert = builder.create();
		errorAlert.show();
	}
	public  boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) this
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	
	private boolean isAlwaysFinishActivitiesOptionEnabled() {
	    int alwaysFinishActivitiesInt = 0;
	    if (Build.VERSION.SDK_INT >= 17) {
	        alwaysFinishActivitiesInt = Settings.System.getInt(getApplicationContext().getContentResolver(), Settings.Global.ALWAYS_FINISH_ACTIVITIES, 0);	        
	    } else {
	        alwaysFinishActivitiesInt = Settings.System.getInt(getApplicationContext().getContentResolver(), Settings.System.ALWAYS_FINISH_ACTIVITIES, 0);	        
	    }

	    if (alwaysFinishActivitiesInt == 1) {
	        return true;
	    } else {
	        return false;
	    }
	}
	private void showDeveloperOptionsScreen(){
		final AlertDialog.Builder builder = new AlertDialog.Builder(MOLPayActivity.this);
		builder.setMessage(
				"Settings -> General -> Developer options -> Apps (Do not keep activities) is enabled. Please click OK to turn it off and click BACK button to resume payment. ?  ")
				.setCancelable(false)
				.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {								 
								alert.dismiss();						
							    Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS);
							    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
							    startActivity(intent);
							}
						})
				.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.dismiss();

					}
				});
		alert = builder.create();
		alert.show();
		
	}

	private void deleteExpiredReceipts(Context ctx)
	{
		try
		{
			SecurePreferences preferences =  new SecurePreferences(ctx, "cash", SECRET_KEY, true);
			preferences.removeAllExpiredReceipts();
		}catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private String executeTxnIdManually() {
		String result = "";

		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); // Timeout
		// Limit
		HttpResponse response;
		JsonObject json = new JsonObject();
		try {
			String combinationMsg = MerchantId + "D7" + OrderId
					+ Float.toString(Amount) + VerifyKey;
			String vcode = Utils.getInstance().string2MD5(combinationMsg);

			URI url = new URI(request);

			HttpPost post = new HttpPost(url);
			json.addProperty("merchant_id", MerchantId);
			json.addProperty("order_ID", OrderId);
			json.addProperty("amount", Float.toString(Amount));
			json.addProperty("chksum", vcode);
			json.addProperty("msgType", "D7");

			Log.i("order ID", MerchantId+" "+ OrderId+" "+Amount+" "+vcode+
					"json "+json.toString());

			post.setHeader("Content-type", "application/json");

			StringEntity se = new StringEntity(json.toString());
			se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			post.setEntity(se);
			response = client.execute(post);

			if (response != null) {
				InputStream in = response.getEntity().getContent();
				result = Utils.getInstance().convertStreamToString(in);
			}

			post.abort();
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private class GetTxnIDManually extends AsyncTask<String, Void, Boolean> {

		HashMap<String, Object> map;
		//		JsonObject jObject = null;
		String resultReturn;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			// TODO Auto-generated method stub
			try {
				resultReturn = executeTxnIdManually();
				//	System.out.println("the result of d7 is "+resultReturn);
				JsonParser parser = new JsonParser();

				// System.out.println("the response is  / "+ response);
				if (!resultReturn.equals("")) {
					map = Utils.getInstance().jsonToMap(new JSONObject(resultReturn));
//					jObject = (JsonObject) parser.parse(resultReturn);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (result) {
				if (map != null) {

					try
					{
						bundle = new Bundle();
						for (Map.Entry<String, Object> entry : map.entrySet()) {
							bundle.putString(entry.getKey(), entry.getValue().toString());
						}
						bundle.putString(MerchantInfo.BILL_NAME, "" + bill_name);
						bundle.putString(MerchantInfo.BILL_EMAIL, "" + bill_email);
						bundle.putString(MerchantInfo.BILL_MOBILE, "" + bill_mobile);
						bundle.putString(MerchantInfo.BILL_DESC, "" +bill_desc);
						bundle.putString(MerchantInfo.CHANNEL, "" + Channel);
						bundle.putString(MerchantInfo.CURRENCY, "" +Currency);
						bundle.putString(MerchantInfo.ORDER_ID, "" + OrderId);
						bundle.putString(MerchantInfo.AMOUNT, "" + Amount);
						bundle.putString(MerchantInfo.ERR_DESC, "Timer is finished");

						try {
							onFinishData(bundle);
						} catch (Exception e) {
							onFinishData(null);
						}
					}
					catch(Exception Ex)
					{
						onFinishData(null);
					}
				} else {
					onFinishData(null);
				}
			}
		}
	}
}
