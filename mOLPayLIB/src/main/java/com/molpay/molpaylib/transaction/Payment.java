package com.molpay.molpaylib.transaction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;

import com.google.gson.JsonObject;

public class Payment {

	private boolean secureMode;
	private float amount;
	private String orderid;
	private String country;
	private String currency;
	private String merchantid;
	private String vcode;
	private String channel;
	private String bill_name = "";
	private String bill_email = "";
	private String bill_mobile = "";
	private String app_name;
	private String bill_desc = "";
	private int pcode;
	private String verifykey;
	private List<String> channels;

	private String request = "https://www.onlinepayment.com.my/MOLPay/API/mobile/index.php";

	public Payment(boolean secureMode, String orderid, String MerchantId, String VerifyKey, String AppName){
		this.secureMode = secureMode;
		this.orderid = orderid;
		this.merchantid = MerchantId;
		this.verifykey = VerifyKey;
		this.app_name = AppName;
		channels = new ArrayList<String>();
		channels.add("Cash-711");
		channels.add("Cash-Esapay");
		channels.add("Cash-SAM");
	}

	public void setAmount(float amount){
		this.amount = amount;
	}

	public void setCountry(String country){
		this.country = country;
	}

	public void setCurrency(String currency){
		this.currency = currency;
	}

	public void setChannel(String channel){
		this.channel = channel;
	}

	public void setBillName(String bill_name){
		this.bill_name = bill_name;
	}

	public void setBillEmail(String bill_email){
		this.bill_email = bill_email;
	}

	public void setBillMobile(String bill_mobile){
		this.bill_mobile = bill_mobile;
	}

	public void setBillDesc(String bill_desc){
		this.bill_desc = bill_desc;
	}

	public void setPCode(int pcode){
		this.pcode = pcode;
	}

	public String[] makePayment(){
		return postPayment();
	}

	private String[] postPayment(){
		
		if(secureMode){
			vcode = verifykey;
		} else{
			String combinationMsg = Float.toString(amount)+merchantid+orderid+verifykey+"B3";
			vcode = string2MD5(combinationMsg);	
		}

		HttpClient client = new DefaultHttpClient();
		client.getParams().setParameter(ClientPNames.HANDLE_REDIRECTS, false);
		client.getParams().setParameter("http.protocol.version",
				HttpVersion.HTTP_1_0);
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); // Timeout
		// Limit
		String result = "";
		HttpResponse response;
		JsonObject json = new JsonObject();
		try {
			URI url = new URI(request);

			HttpPost post = new HttpPost(url);
			
		//	System.out.println("amount: "+amount);
			//System.out.println("orderid: "+orderid);
			//System.out.println("country: "+country);
			//System.out.println("cur: "+currency);
			//System.out.println("merchant_id: "+merchantid);
			//System.out.println("channel: "+channel);
			
			json.addProperty("amount", Float.toString(amount));
			json.addProperty("orderid", orderid);
			json.addProperty("country", country);
			json.addProperty("cur", currency);
			json.addProperty("merchant_id", merchantid);
			json.addProperty("channel", channel);
			json.addProperty("bill_name", bill_name);
			json.addProperty("bill_email", bill_email);
			json.addProperty("bill_mobile", bill_mobile);
			json.addProperty("app_name", app_name);
			json.addProperty("bill_desc", bill_desc);
			json.addProperty("msgType","B3");
			json.addProperty("vcode", vcode);
			if(channels.contains(channel)){
				json.addProperty("pcode", pcode);
			}

			post.setHeader("Content-type", "application/json");

			StringEntity se = new StringEntity(json.toString());
			se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			post.setEntity(se);
			response = client.execute(post);

			if (response != null) {
				InputStream in = response.getEntity().getContent(); 
				result = convertStreamToString(in);

				post.abort();

				String[] webData = {result, post.getURI().toString()};
				
				return webData;
			}else{
				post.abort();
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private String string2MD5(String md5) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes("UTF-8"));
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
		} catch (UnsupportedEncodingException ex){
		}
		return null;
	}

	public static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
}
