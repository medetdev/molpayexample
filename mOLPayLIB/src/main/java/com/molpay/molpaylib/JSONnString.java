package com.molpay.molpaylib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.molpay.molpaylib.settings.MerchantInfo;
import com.molpay.molpaylib.utilities.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

public class JSONnString {

	// OrderInfo
	private static String OrderId;
	private static float Amount;
	private static String Country;
	private static String Currency;
	private static String bill_name;
	private static String bill_desc;
	private static String bill_email;
	private static String bill_mobile;
	private static String cvv = "";
	private static int Filter = 0;
	private static String USERNAME;
	private static String PASSWORD;

	// MerchantInfo
	private static String MerchantId;
	private static String VerifyKey;
	private static String AppName;

	private static String cardNo = "";
	private static String month = "";
	private static String year = "";
	private static String bankName = "";
	private static String Token = "";
	private static String channel = "";

	private static boolean is_escrow;

	private static String request = "https://www.onlinepayment.com.my/MOLPay/API/mobile_new/index.php";
	private static boolean secureMode;

	public static String GetTxnIdManually(String MerchantId,String order,Float Amount,String VerifyKey) {
		String result = "";

		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); // Timeout
		// Limit
		HttpResponse response;
		JsonObject json = new JsonObject();
		try {
			String combinationMsg = MerchantId + "D7" + order
					+ Float.toString(Amount) + VerifyKey;
			String vcode = string2MD5(combinationMsg);

			URI url = new URI(request);

			HttpPost post = new HttpPost(url);
			json.addProperty("merchant_id", MerchantId);
			json.addProperty("order_ID", order);
			json.addProperty("amount", Float.toString(Amount));
			json.addProperty("chksum", vcode);
			json.addProperty("msgType", "D7");

			post.setHeader("Content-type", "application/json");

			StringEntity se = new StringEntity(json.toString());
			se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			post.setEntity(se);
			response = client.execute(post);

			if (response != null) {
				InputStream in = response.getEntity().getContent();
				result = convertStreamToString(in);
			}
			post.abort();



			return result;



		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	public static String token(String url, Bundle extras) {
		String result = null;

		if (extras != null) {
			MerchantId = extras.getString("MerchantId");
			AppName = extras.getString("AppName");
			VerifyKey = extras.getString("VerifyKey");
			OrderId = extras.getString("OrderId").trim();
			bill_name = extras.getString("BillName");
			bill_mobile = extras.getString("BillMobile");
			bill_email = extras.getString("BillEmail");
			Filter = extras.getInt("filter");
			Amount = extras.getFloat("Amount");
			Country = extras.getString("Country").trim();
			channel = extras.getString("Channel").trim();
			Currency = extras.getString("Currency").trim();
			USERNAME = extras.getString("Username");
			PASSWORD = extras.getString("Password");
		}

		result = executeToken("https://www.onlinepayment.com.my/MOLPay/API/chips/index.php");
		return result;
	}

	public static String debug(String url, Bundle extras) {
		String result = null;

		if (extras != null) {


			MerchantId = extras.getString("MerchantId").trim();
			AppName = extras.getString("AppName").trim();
			VerifyKey = extras.getString("VerifyKey").trim();
			OrderId = extras.getString("OrderId").trim();
			bill_name = extras.getString("BillName").trim();
			bill_desc = extras.getString("BillDesc").trim();
			bill_mobile = extras.getString("BillMobile").trim();
			bill_email = extras.getString("BillEmail").trim();
			channel = extras.getString("Channel").trim();
			Currency = extras.getString("Currency").trim();
			Country = extras.getString("Country").trim();
			Amount = extras.getFloat("Amount");
			USERNAME = extras.getString("Username");
			PASSWORD = extras.getString("Password");
		}

		result = executeDebug("https://www.onlinepayment.com.my/MOLPay/API/InternalUsed/index.php?mod=mobile&opt=Payment/recordsLog");

		return result;
	}

	private static String executeToken(String request) {

		InputStream is = null;

		String result = "";
		JSONObject json = new JSONObject();
		try {
			json.put("merchant_id", MerchantId);
			json.put("bill_name", bill_name);
			json.put("bill_email", bill_email);
			json.put("bill_mobile", bill_mobile);
			json.put("app_name", AppName);
			json.put("msgType", "T1");
			json.put("filter", "" + Filter);
			json.put("amount", "" + String.valueOf(Amount));
			json.put("orderid", "" + OrderId);

		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


		String verifyH1 = string2MD5(VerifyKey);
		String checkPass = sha1Hash(MerchantId + AppName + verifyH1);
		String paramsSha1 = sha1Hash(jsonIterator(json));
		String checksum = (paramsSha1 + checkPass);

		HttpClient client = new DefaultHttpClient();
		((AbstractHttpClient) client).getCredentialsProvider().setCredentials(
				new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT),
				new UsernamePasswordCredentials("" + USERNAME, "" + PASSWORD));
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); // Timeout
		HttpPost post = new HttpPost(request);

		// set values you'd like to send
		List<NameValuePair> pairs = new ArrayList<NameValuePair>(2);
		pairs.add(new BasicNameValuePair("Checksum", checksum));
		pairs.add(new BasicNameValuePair("Params", json.toString()));

		try {
			post.setEntity(new UrlEncodedFormEntity(pairs, "UTF-8"));
			HttpResponse httpResponse = client.execute(post);
			HttpEntity httpEntity = httpResponse.getEntity();

			// read content
			is = httpEntity.getContent();

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();
			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		return result;
	}

	private static String executeDebug(String request) {

		InputStream is = null;

		String result = "";
		JSONObject json = new JSONObject();
		try {
			json.put("merchantID", MerchantId);

			String message="Merchant ID : "+MerchantId+" , App Name : "+AppName+", VerifyKey : "+VerifyKey+" , Order ID : "+OrderId+" , Bill Name : "+bill_name+" , Bill Desc : "+bill_desc+" , Bill Mobile : "+bill_mobile+" , Bill Email : "+bill_email+" , Channel : "+channel+" , Currency :"+Currency+" , Country : "+Country+" , Amount :"+Amount;

			message+=" Android Version : "+	Build.VERSION.RELEASE +" , Device : "+android.os.Build.DEVICE+" , Model : "+ 	android.os.Build.MODEL +" , Product : "+ android.os.Build.PRODUCT ;
			json.put("message", message);


		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String verifyH1 = string2MD5(VerifyKey);
		String checkPass = sha1Hash(MerchantId + AppName + verifyH1);
		String paramsSha1 = jsonIterator(json);

		String checksum =  sha1Hash(paramsSha1 + checkPass);

		HttpClient client = new DefaultHttpClient();
		((AbstractHttpClient) client).getCredentialsProvider().setCredentials(
				new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT),
				new UsernamePasswordCredentials("" + USERNAME, "" + PASSWORD));
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); // Timeout
		HttpPost post = new HttpPost(request);

		// set values you'd like to send
		List<NameValuePair> pairs = new ArrayList<NameValuePair>(2);
		pairs.add(new BasicNameValuePair("Checksum", checksum));
		pairs.add(new BasicNameValuePair("Params", json.toString()));

		try {
			post.setEntity(new UrlEncodedFormEntity(pairs));
			HttpResponse httpResponse = client.execute(post);
			HttpEntity httpEntity = httpResponse.getEntity();

			// read content
			is = httpEntity.getContent();

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();
			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		return result;
	}

	public static String paymentChannel(Bundle extras) {
		String result = null;

		if (extras != null) {
			MerchantId = extras.getString("MerchantId");
			AppName = extras.getString("AppName");
			VerifyKey = extras.getString("VerifyKey");
			OrderId = extras.getString("OrderId");
			bill_name = extras.getString("BillName");
			bill_desc = extras.getString("BillDesc");
			bill_mobile = extras.getString("BillMobile");
			bill_email = extras.getString("BillEmail");
			Currency = extras.getString("Currency");
			Country = extras.getString("Country");
			Amount = extras.getFloat("Amount");
			USERNAME = extras.getString("Username");
			PASSWORD = extras.getString("Password");
		}
		result = executepaymentChannel("https://www.onlinepayment.com.my/MOLPay/API/InternalUsed/index.php?mod=mobile&opt=Payment/GetAvailableChannelById");
		return result;
	}

	private static String executepaymentChannel(String request) throws NullPointerException {

		InputStream is = null;

		String result = "";
		JSONObject json = new JSONObject();
		try {
			json.put("merchantID", MerchantId);
			json.put("currency", Currency);
			json.put("amount", String.valueOf(Amount));
			json.put("orderid", OrderId);

		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String paramsSha1 = jsonIterator(json);
		String verifyH1 = string2MD5(VerifyKey);
		String checkPass = sha1Hash(MerchantId + AppName + verifyH1);
		String checksum = sha1Hash(paramsSha1 + checkPass);

		HttpClient client = new DefaultHttpClient();
		((AbstractHttpClient) client).getCredentialsProvider().setCredentials(
				new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT),
				new UsernamePasswordCredentials("" + USERNAME, "" + PASSWORD));
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); // Timeout
		HttpPost post = new HttpPost(request);

		// set values you'd like to send
		List<NameValuePair> pairs = new ArrayList<NameValuePair>(2);
		pairs.add(new BasicNameValuePair("Checksum", checksum));
		pairs.add(new BasicNameValuePair("Params", json.toString()));

		try {
			post.setEntity(new UrlEncodedFormEntity(pairs));
			HttpResponse httpResponse = client.execute(post);
			HttpEntity httpEntity = httpResponse.getEntity();

			// read content
			is = httpEntity.getContent();

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();
			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		return result;
	}

	private static String string2MD5(String md5) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes("UTF-8"));
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
						.substring(1, 3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
		} catch (UnsupportedEncodingException ex) {
		}
		return null;
	}

	private static String sha1Hash(String toHash) {
		String hash = null;
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-1");
			byte[] bytes = toHash.getBytes("UTF-8");
			digest.update(bytes, 0, bytes.length);
			bytes = digest.digest();

			// This is ~55x faster than looping and String.formating()
			hash = bytesToHex(bytes);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return hash;
	}

	// http://stackoverflow.com/questions/9655181/convert-from-byte-array-to-hex-string-in-java
	final protected static char[] hexArray = "0123456789abcdef".toCharArray();

	public static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	public static String paymentHttp(Bundle extras, String jArray, String addCvv) {

		if (extras != null) {
			secureMode = extras.getBoolean("IsSecure", false);
			MerchantId = extras.getString("MerchantId");
			AppName = extras.getString("AppName");
			VerifyKey = extras.getString("VerifyKey");
			OrderId = extras.getString("OrderId");
			bill_name = extras.getString("BillName");
			bill_desc = extras.getString("BillDesc");
			bill_mobile = extras.getString("BillMobile");
			bill_email = extras.getString("BillEmail");
			Currency = extras.getString("Currency");
			Country = extras.getString("Country");
			Amount = extras.getFloat("Amount");
			is_escrow=extras.getBoolean("is_escrow");
			channel = extras.getString("Channel");
		}

		if (jArray.length() > 0) {

			JSONArray jA;
			try {
				jA = new JSONArray(jArray);

				JSONObject jO = jA.getJSONObject(0);
				bankName = (jO.getString("issuer_bank"));

				if (!jO.isNull("cardnumber")) {
					cardNo = (jO.getString("cardnumber"));
				}

				if (!jO.isNull("token")) {
					Token = (jO.getString("token"));
				}

				String mmyy = jO.getString("expdate");

				if (mmyy.length() > 4) {
					year = mmyy.substring(2, 4);
					month = mmyy.substring(4, 6);
				} else {
					year = mmyy.substring(0, 2);
					month = mmyy.substring(2, 4);
				}

				if (!jO.isNull("bank_country")) {
					Country = jO.getString("bank_country");
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			cvv = addCvv;
		} else {
			channel = addCvv;

		}

		return payHttpPost();
	}

	public static String paymentMolwallet(Bundle extras, String jArray, String addCvv) {

		if (extras != null) {
			MerchantId = extras.getString("MerchantId");
			AppName = extras.getString("AppName");
			VerifyKey = extras.getString("VerifyKey");
			OrderId = extras.getString("OrderId");
			bill_name = extras.getString("BillName");
			bill_desc = extras.getString("BillDesc");
			bill_mobile = extras.getString("BillMobile");
			bill_email = extras.getString("BillEmail");
			Currency = extras.getString("Currency");
			Country = extras.getString("Country");
			Amount = extras.getFloat("Amount");
			is_escrow=extras.getBoolean("is_escrow");
		}


		return payMolwalletPost();
	}

	private static String payMolwalletPost() {
		String result = "";

		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
		// Timeout
		// Limit
		HttpResponse response;
		JSONObject json = new JSONObject();
		try {
			String combinationMsg = Float.toString(Amount) + MerchantId
					+ OrderId + VerifyKey + "E9";
			String vcode = string2MD5(combinationMsg);
			URI url = new URI(
					"https://www.onlinepayment.com.my/MOLPay/API/mobile_new/index.php");
			HttpPost post = new HttpPost(url);

			try {
				json.put("amount", "" + Amount);
				json.put("orderid", OrderId);
				json.put("country", Country);
				json.put("cur", Currency);
				json.put("merchant_id", MerchantId);
				json.put("vcode", vcode);
				json.put("channel", "" + "MOLWallet");
				json.put("bill_name", bill_name);
				json.put("bill_email", bill_email);
				json.put("bill_mobile", bill_mobile);
				json.put("app_name", AppName);
				json.put("bill_desc", bill_desc);
				json.put("l_version", "2");
				json.put("msgType", "E9");

			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			post.setHeader("Content-type", "application/json");

			StringEntity se = new StringEntity(json.toString());
			se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			post.setEntity(se);

			response = client.execute(post);

			if (response != null) {
				InputStream in = response.getEntity().getContent();
				result = convertStreamToString(in);
			}

			post.abort();
			return result;


		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private static String payHttpPost() {
		String result = "";

		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
		// Timeout
		// Limit
		HttpResponse response;
		JSONObject json = new JSONObject();
		try {
			String vcode = "";
			if(secureMode){
				vcode = VerifyKey;
			} else{
				String combinationMsg = "" + Amount + MerchantId
						+ OrderId + VerifyKey + "B3";
				vcode = string2MD5(combinationMsg);
			}

			URI url = new URI(
					"https://www.onlinepayment.com.my/MOLPay/API/mobile_new/index.php");
			HttpPost post = new HttpPost(url);

			try {
				json.put("cardnumber", "" + cardNo);
				json.put("cvv", cvv);
				json.put("month", "" + month);
				json.put("year", "" + year);
				json.put("bank_name", bankName);
				if (Token.length() > 0) {
					json.put("token", "" + Token);
				}
				json.put("amount", "" + Float.toString(Amount));
				json.put("orderid", OrderId);
				json.put("country", Country);
				json.put("cur", Currency);
				json.put("merchant_id", MerchantId);
				json.put("vcode", vcode);
				if (channel.equals("add_card") || channel.equals("list_card")) channel = "credit";
				json.put("channel", "" + channel);
				json.put("bill_name", bill_name);
				json.put("bill_email", bill_email);
				json.put("bill_mobile", bill_mobile);
				json.put("app_name", AppName);
				json.put("bill_desc", bill_desc);
				json.put("l_version", "2");
				json.put("msgType", "B3");
				json.put("token_status", "1");
				json.put("is_escrow", is_escrow);

			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			post.setHeader("Content-type", "application/json; charset=utf-8");

			StringEntity se = new StringEntity(json.toString(), "utf-8");

			se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,"application/json; charset=utf-8"));

			post.setEntity(se);

			response = client.execute(post);

			if (response != null) {
				InputStream in = response.getEntity().getContent();
				result = convertStreamToString(in);
				in.close();
			}

			post.abort();
			return result;


		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String requestResult(String merchantID, String txnID,
									   float amount, String verifyKey) {
		return requestResult(merchantID, txnID, amount,verifyKey, false);
	}

	public static String requestResult(String merchantID, String txnID,
									   float amount, String verifyKey, boolean dontSendEmail) {
		String result = requestResultHttp(merchantID, txnID, amount, verifyKey, dontSendEmail);

		try {
			JSONObject jsonObj = Utils.getInstance().filterJsonObject(new JSONObject(result));
			result = jsonObj.toString();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	private static String requestResultHttp(String merchantID, String txnID,
											float amount, String verifyKey) {
		return requestResultHttp(merchantID, txnID, amount, verifyKey, false);
	}

	private static String requestResultHttp(String merchantID, String txnID,
											float amount, String verifyKey, boolean dontSendEmail) {
		String result = "";
		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
		// Timeout
		// Limit
		HttpResponse response;
		JSONObject json = new JSONObject();
		String amt = Float.toString(amount);
		try {
			String combinationMsg = merchantID + "C5" + txnID + amt + verifyKey;
			String vcode = string2MD5(combinationMsg);
			URI url = new URI(
					"https://www.onlinepayment.com.my/MOLPay/API/mobile_new/index.php");
			HttpPost post = new HttpPost(url);
			try {
				json.put("merchant_id", "" + merchantID);
				json.put("txn_ID", "" + txnID);
				json.put("amount", "" + amt);
				json.put("chksum", "" + vcode);
				json.put("msgType", "C5");
				if (dontSendEmail) json.put("no_email_notification", dontSendEmail);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			post.setHeader("Content-type", "application/json");

			StringEntity se = new StringEntity(json.toString());
			se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			post.setEntity(se);
			response = client.execute(post);
			if (response != null) {
				InputStream in = response.getEntity().getContent();
				result = convertStreamToString(in);
				in.close();
			}

			post.abort();
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String printAmount(Float Amount) {
		String amt = null;
		if (Amount < 1000) {
			amt = "" + Amount;

			String[] am = amt.split("\\.");
			String a = am[0];
			String b = am[1];

			if (b.length() == 2) {
				amt = a + "." + b;
			} else if (b.length() == 1) {
				amt = a + "." + b + "0";
			} else {
				String[] arr = b.split("(?<!^)");

				String last = arr[0] + arr[1];

				amt = a + "." + last;
			}
		} else if (Amount >= 1000 && Amount <= 9999) {
			amt = "" + Amount;
			String[] am = amt.split("\\.");
			String thousand = am[0].substring(0, 1);
			String hundread = am[0].substring(1);
			String b = am[1];
			if (b.length() == 2) {
				amt = thousand + "," + hundread + "." + am[1];
			} else if (b.length() == 1) {
				amt = thousand + "," + hundread + "." + am[1] + "0";
			} else {
				String[] arr = b.split("(?<!^)");

				String last = arr[0] + arr[1];

				amt = thousand + "," + hundread + "." + last;
			}

		} else if (Amount >= 10000 && Amount <= 99999) {
			amt = "" + Amount;
			String[] am = amt.split("\\.");
			String thousand = am[0].substring(0, 2);
			String hundread = am[0].substring(2);
			String b = am[1];
			if (b.length() == 2) {
				amt = thousand + "," + hundread + "." + am[1];
			} else if (b.length() == 1) {
				amt = thousand + "," + hundread + "." + am[1] + "0";
			} else {
				String[] arr = b.split("(?<!^)");

				String last = arr[0] + arr[1];

				amt = thousand + "," + hundread + "." + last;
			}
		} else {
			amt = "" + Amount;
		}
		return amt;
	}


	public static String toRupiahFormat(String nominal,Boolean noDec) {
		
		/* Double harga=Double.valueOf(nominal);
		    DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
		    
	        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

	        formatRp.setCurrencySymbol("");
	         
	        formatRp.setMonetaryDecimalSeparator(',');
	        formatRp.setGroupingSeparator('.');
	       

	         kursIndonesia.setDecimalFormatSymbols(formatRp);
	        return kursIndonesia.format(harga);*/

		Double money = Double.valueOf(nominal);
		String strFormat="";
		if(noDec)
		{
			strFormat ="#,###";
		}
		else
		{
			strFormat ="#,###.00";
		}


		DecimalFormat df = new DecimalFormat(strFormat,new DecimalFormatSymbols(Locale.GERMAN));
		return df.format(money);
	}


	private static String jsonIterator(JSONObject json) {

		Iterator<?> keys = json.keys();

		String chksumstr = "";

		while (keys.hasNext()) {
			String key = (String) keys.next();
			try {
				chksumstr = chksumstr.concat(json.getString(key));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return chksumstr;
	}
}