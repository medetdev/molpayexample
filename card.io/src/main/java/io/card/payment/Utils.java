package io.card.payment;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Utils {
	private static Utils utils;
	private String generatedCode;
	private String codeStringValue;

	public static Utils getInstance()
	{
		if(utils == null){
			utils = new Utils();
		}
		
		return utils;
	}
	
	public enum ResourceType {
		layout, id, drawable, anim, array;
	}
	
	public int getResource(Context context, String resourceName, ResourceType resourceType)
	{
		return context.getResources().getIdentifier(resourceName, resourceType.toString(), context.getPackageName());
	}

	private String getFullCode()
	{

		int chetVal = 0, nechetVal = 0;
		String codeToParse = codeStringValue;

		for( int index = 0;index<6;index++ )
		{
			chetVal += Integer.valueOf(codeToParse.substring(
					index*2+1,index*2+2)).intValue();
			nechetVal += Integer.valueOf(codeToParse.substring(
					index*2,index*2+1)).intValue();
		}

		chetVal *= 3;
		int controlNumber = 10 - (chetVal+nechetVal)%10;
		if( controlNumber == 10 ) controlNumber  = 0;

		codeToParse += String.valueOf(controlNumber);

		return codeToParse;

	}

	private String createEAN13Code(String rawCode)
	{
		int firstFlag = Integer.valueOf(

				rawCode.substring(0,1)

		).intValue();

		String leftString = rawCode.substring(1,7);
		String rightString = rawCode.substring(7);

		String rightCode = "";
		String leftCode = "";

		for( int i=0;i<6;i++)
		{
			rightCode += DigitToLowerCase( rightString.substring(i,i+1) );
		}



		if( firstFlag == 0 )
		{
			leftCode = "#!"+leftString.substring(0,1)
					+leftString.substring(1,2)
					+leftString.substring(2,3)
					+leftString.substring(3,4)
					+leftString.substring(4,5)
					+leftString.substring(5);
		}
		if( firstFlag == 1 )
		{

			leftCode = "$!"+leftString.substring(0,1)
					+leftString.substring(1,2)
					+DigitToUpperCase(leftString.substring(2,3))
					+leftString.substring(3,4)
					+DigitToUpperCase(leftString.substring(4,5))
					+DigitToUpperCase(leftString.substring(5));
		}
		if( firstFlag == 2 )
		{
			leftCode = "%!"+leftString.substring(0,1)
					+leftString.substring(1,2)
					+DigitToUpperCase(leftString.substring(2,3))
					+DigitToUpperCase(leftString.substring(3,4))
					+leftString.substring(4,5)
					+DigitToUpperCase(leftString.substring(5));
		}
		if( firstFlag == 3 )
		{
			leftCode = "&!"+leftString.substring(0,1)
					+leftString.substring(1,2)
					+DigitToUpperCase(leftString.substring(2,3))
					+DigitToUpperCase(leftString.substring(3,4))
					+DigitToUpperCase(leftString.substring(4,5))
					+leftString.substring(5);
		}
		if( firstFlag == 4 )
		{
			leftCode = "'!"+leftString.substring(0,1)
					+DigitToUpperCase(leftString.substring(1,2))
					+leftString.substring(2,3)
					+leftString.substring(3,4)
					+DigitToUpperCase(leftString.substring(4,5))
					+DigitToUpperCase(leftString.substring(5));
		}
		if( firstFlag == 5 )
		{
			leftCode = "(!"+leftString.substring(0,1)
					+DigitToUpperCase(leftString.substring(1,2))
					+DigitToUpperCase(leftString.substring(2,3))
					+leftString.substring(3,4)
					+leftString.substring(4,5)
					+DigitToUpperCase(leftString.substring(5));
		}
		if( firstFlag == 6 )
		{
			leftCode = ")!"+leftString.substring(0,1)
					+DigitToUpperCase(leftString.substring(1,2))
					+DigitToUpperCase(leftString.substring(2,3))
					+DigitToUpperCase(leftString.substring(3,4))
					+leftString.substring(4,5)
					+leftString.substring(5);
		}
		if( firstFlag == 7 )
		{
			leftCode = "*!"+leftString.substring(0,1)
					+DigitToUpperCase(leftString.substring(1,2))
					+leftString.substring(2,3)
					+DigitToUpperCase(leftString.substring(3,4))
					+leftString.substring(4,5)
					+DigitToUpperCase(leftString.substring(5));
		}
		if( firstFlag == 8 )
		{
			leftCode = "+!"+leftString.substring(0,1)
					+DigitToUpperCase(leftString.substring(1,2))
					+leftString.substring(2,3)
					+DigitToUpperCase(leftString.substring(3,4))
					+DigitToUpperCase(leftString.substring(4,5))
					+leftString.substring(5);
		}
		if( firstFlag == 9 )
		{
			leftCode = ",!"+leftString.substring(0,1)
					+DigitToUpperCase(leftString.substring(1,2))
					+DigitToUpperCase(leftString.substring(2,3))
					+leftString.substring(3,4)
					+DigitToUpperCase(leftString.substring(4,5))
					+leftString.substring(5);
		}



		String retVal = leftCode + "-" + rightCode + "!";

		return retVal;
	}

	private void parse()
	{
		String fullString = getFullCode();

		generatedCode = createEAN13Code(fullString);
	}

	private String DigitToUpperCase( String digit)
	{
		String letters  = "ABCDEFGHIJ";
		int position = Integer.valueOf(digit).intValue();

		String retVal = letters.substring(position,position+1);

		return retVal;

	}

	private String DigitToLowerCase( String digit)
	{
		String letters  = "abcdefghij";
		int position = Integer.valueOf(digit).intValue();

		String retVal = letters.substring(position,position+1);

		return retVal;

	}

	public static HashMap<String, Object> jsonToMap(String jsonString) throws JSONException {
		JSONObject json = new JSONObject(jsonString);
		HashMap<String, Object> retMap = new HashMap<String, Object>();
		if(json != JSONObject.NULL) {
			retMap = toMap(json);
		}
		return retMap;
	}

	public static HashMap<String, Object> toMap(JSONObject object) throws JSONException {
		HashMap<String, Object> map = new HashMap<String, Object>();

		Iterator<String> keysItr = object.keys();
		while(keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = object.get(key);

			if(key.equals("chksum") || key.equals("msgType"))
				continue;

			if(value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if(value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			map.put(key, value);
		}
		return map;
	}

	public static List<Object> toList(JSONArray array) throws JSONException {
		List<Object> list = new ArrayList<Object>();
		for(int i = 0; i < array.length(); i++) {
			Object value = array.get(i);
			if(value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if(value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			list.add(value);
		}
		return list;
	}

	public static JSONObject filterJsonObject(JSONObject object) throws JSONException {
		JSONObject jsonObject = new JSONObject();

		Iterator<String> keysItr = object.keys();
		while(keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = object.get(key);

			if(key.equals("chksum") || key.equals("msgType"))
				continue;

			jsonObject.put(key, value);
		}
		return jsonObject;
	}
}
