package com.molpay.molpayexample;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.molpay.molpaylib.CashReceiptActivity;
import com.molpay.molpaylib.JSONnString;
import com.molpay.molpaylib.MOLPayActivity;
import com.molpay.molpaylib.settings.MerchantInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Random;


public class MainActivity extends Activity {
	//private TextView result;
	public static final int REQUEST_CODE = 1; 
	Button pay;
	RelativeLayout result;
	TextView name;
	TextView mobile;
	TextView email;
	TextView desc;
	TextView trans;
	TextView amt;
	TextView stat;
	LinearLayout main;
	private HashMap<String, String> map;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		 
		setContentView(R.layout.activity_main);
		main=(LinearLayout) findViewById(R.id.main);
		result=(RelativeLayout) findViewById(R.id.result);
	 
		
		
        name=(TextView) findViewById(R.id.nameTxt);
        mobile=(TextView) findViewById(R.id.mobileTxt);
        email=(TextView) findViewById(R.id.emailTxt);
        desc=(TextView) findViewById(R.id.descTxt);
        trans=(TextView) findViewById(R.id.transTxt);
        amt=(TextView) findViewById(R.id.amtTxt);
        stat=(TextView) findViewById(R.id.statTxt);
       
       
       
		
		pay=(Button) findViewById(R.id.pay);

		pay.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this,MOLPayActivity.class);
//				Intent intent = new Intent(MainActivity.this,CashReceiptActivity.class);
				Bundle b = new Bundle();

				/*b.putString("MerchantId", "MyCompany_Dev");
				b.putString("AppName", "MyCompany_Dev_App");
				b.putString("VerifyKey", "b2d0f925f54efa933a3f7b80a7d37b43");
				b.putString("Username", "molpayapi");
				b.putString("Password", "*M0Lp4y4p1!*");*/

				b.putString("MerchantId", "molpaymerchant");
				b.putString("AppName", "wilwe_makan2");
				b.putString("VerifyKey", "501c4f508cf1c3f486f4f5c820591f41");
				b.putString("Username", "molpayapi");
				b.putString("Password", "*M0Lp4y4p1!*");

//				b.putString("MerchantId", "gameview");
//				b.putString("AppName", "MfaceMolPay");
//				b.putString("VerifyKey", "b23459ebbc660bfaf6e89fdaa5052971");
//				b.putString("Username", "molpayapi");
//				b.putString("Password", "*M0Lp4y4p1!*");

//				b.putString("MerchantId", "MOLPayRND");
//				b.putString("AppName", "Avenue86");
//				b.putString("VerifyKey", "103924635c13f8c5791a5f116526614e");
//				b.putString("Username", "molpayapi");
//				b.putString("Password", "*M0Lp4y4p1!*");

				float amount =  1.10f;
//
				Random r = new Random();
				int i1 = r.nextInt(500000 - 1) + 1;
				String orderId = "GPAA" + String.valueOf(i1);
//				b.putString("OrderId", "");
                b.putString("OrderId", orderId);
//				b.putString("TransactionId", "4860396");

				/*String combinationMsg = String.valueOf (amount) + "MyCompany_Dev"+orderId+"b2d0f925f54efa933a3f7b80a7d37b43"+"B3";
				String vcode = string2MD5(combinationMsg);
				b.putString("VerifyKey", vcode);

				b.putBoolean("IsSecure", true);*/

				b.putString("BillName", "Demo Name");
				b.putString("BillDesc", "500 gold");//"Purchase of 5 pcs of survivor kits");
				b.putString("BillMobile", "6012712312321");
//				b.putString("BillEmail", "DemoName@mail.my");
				b.putString("BillEmail", "russell.li@imaginato.com");
				b.putString("Channel", "multi");
//				b.putString("Channel", "paymentasia");
//				b.putString("Channel", "paysbuy");
				b.putString("Currency", "MYR");
//				b.putString("Currency", "CNY");
//				b.putString("Currency", "IDR");
//				b.putString("Currency", "THB");
				b.putString("Country", "MY");
				b.putFloat("Amount", amount);
				b.putBoolean("debug",false);
				b.putBoolean("editable", true);
				b.putBoolean("ShowCCDetails", true);
				b.putString("DefaultCountry", "Malaysia");
				b.putBoolean("ShowTimer", true);
				b.putLong("Duration", 60 * 1000);

				intent.putExtras(b);
				startActivityForResult(intent, REQUEST_CODE);


				// String verifyKey = vcode;
				//String [] request = {"MyCompany_Dev", orderId, String.valueOf (amount), verifyKey};
				//new RequestResult().execute(request);
//				final String merchantId = "hstore_Dev";
//				final String transactionId = "4691281";
//				final float amount = 1.1f;
//				final String verifyKey = "acc06894c07b114b845c0ce788947041";
//				String result = JSONnString.requestResult(merchantId, transactionId, amount, verifyKey);
//				System.out.println("Result==>"+result);
			}
		});
	}
	
	private String string2MD5(String md5) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes("UTF-8"));
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
		} catch (UnsupportedEncodingException ex){
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		try{
			if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
				main.setVisibility(View.GONE);
				result.setVisibility(View.VISIBLE);

				Bundle bundle = data.getExtras().getBundle("bundle");

				if (bundle != null) {
					String error_desc = bundle.getString(MerchantInfo.ERR_DESC);
					String amount = bundle.getString(MerchantInfo.PAY_AMOUNT);
					String transaction_id = bundle.getString(MerchantInfo.TXN_ID);
					String transaction_status = bundle
							.getString(MerchantInfo.STATUS_CODE);
					String bill_name=bundle.getString(MerchantInfo.BILL_NAME);
					String bill_email=bundle.getString(MerchantInfo.BILL_EMAIL);
					String bill_mobile=bundle.getString(MerchantInfo.BILL_MOBILE);
					String bill_desc=bundle.getString(MerchantInfo.BILL_DESC);
					String channel = bundle.getString(MerchantInfo.CHANNEL);

					name.setText(bill_name);
					email.setText(bill_email);
					mobile.setText(bill_mobile);
					desc.setText(bill_desc);
					trans.setText(transaction_id);
					amt.setText("MYR "+amount);




					//String Result = "the name is "+bill_name+"\nthe mobile is " + bill_mobile +"\nthe email is " + bill_email +"\nthe description is " + bill_desc +"\nthe amount is " + amount + "\nthe transaction id is " + transaction_id ;

					if(transaction_status.equals("00"))
					{
						stat.setText("Success");
					}
					else if(transaction_status.equals("11"))
					{
						stat.setText("Failed");
					}
					else if(transaction_status.equals("22"))
					{
						stat.setText("Pending");
					}

					//result.setText(""+Result);
				}
			}
		}catch (Exception ex){

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	 private boolean isNetworkAvailable() {
		    ConnectivityManager connectivityManager 
		          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	 }
	 
	 
	 private class RequestResult extends AsyncTask<String, Void, Boolean> { String HTMLResult;
	 @Override
	 protected Boolean doInBackground(String... param) {
	 String merchant_id = ""+param[0]; String order_id = ""+param[1];
	 float amount = Float.parseFloat(param[2]); String verifyKey =""+param[3];
	 HTMLResult = JSONnString.GetTxnIdManually(merchant_id, order_id, amount, verifyKey);
	 return true;
	 }
	 @Override
	 protected void onPostExecute(Boolean result) {
	 super.onPostExecute(result);
	 if (result)
	 {
	 try {
	 JSONObject jsonObj = new JSONObject(HTMLResult);
	 String transactionID = jsonObj.getString("txn_ID"); String amount = jsonObj.getString("amount");
	 String status_code = jsonObj.getString("status_code"); String app_code = jsonObj.getString("app_code");
	 String pay_date = jsonObj.getString("paydate");
	 }
	 catch (JSONException e) {
	 e.printStackTrace();
	 }
	 }
	 }
	 }

}
